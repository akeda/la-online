<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "study_program_id" => array(
                    'title' => 'Prodi',
                    'sortable' => true
                ),
                "name" => array(
                    'title' => __("Name", true), 
                    'sortable' => true
                ),
                "year_started" => array(
                    'title' => 'Tahun masuk / angkatan', 
                    'sortable' => true
                )
              ),
              "editable"  => "name",
              "assoc" => array(
                "study_program_id" => array(
                    'model' => 'StudyProgram',
                    'field' => 'name'
                )
            )
        ));
?>
</div>
