<div class="<?=$this->params['controller']?> <?=$html->action?>">
<form method="get" action="#">
	<fieldset>
 		<legend><?php echo 'Rekapitulasi Pembayaran Mahasiswa';?></legend>
        <table class="input">
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Kelas');?>:</td>
                <td>
                    <?php
                        echo $form->select('class', $student_classes, null, array(
                            'name'  => 'class',
                            'empty' => false, 'id' => 'class'
                        ));
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
