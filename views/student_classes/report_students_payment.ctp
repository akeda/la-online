<style type="text/css">
th {
    text-align: center;
}
</style>
<h1>
REKAP PEMBAYARAN SPP DAN DPP<br />MAHASISWA ANGKATAN <?php echo $class['StudentClass']['year_started'];?><br />
PRODI : <?php echo $class['StudentClass']['name'];?>
</h1>
<table>
    <thead>
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">NAMA</th>
            <th rowspan="2">NIM</th>
            <th colspan="14">PEMBAYARAN UANG KULIAH</th>
            <th rowspan="2">JUMLAH YG<br />SUDAH DIBAYAR</th>
            <th rowspan="2">JUMLAH YG<br />BLM DIBAYAR</th>
            <th rowspan="2">KETERANGAN</th>
        </tr>
        <tr>
            <th>TGL<br />BAYAR</th>
            <th>SMT I</th>
            <th>TGL<br />BAYAR</th>
            <th>SMT II</th>
            <th>TGL<br />BAYAR</th>
            <th>SMT III</th>
            <th>TGL<br />BAYAR</th>
            <th>SMT IV</th>
            <th>TGL<br />BAYAR</th>
            <th>SMT V</th>
            <th>TGL<br />BAYAR</th>
            <th>SMT VI</th>
            <th>TGL<br />BAYAR</th>
            <th>DPP</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($payments as $key => $s):?>
        <tr>
            <td><?php echo $key + 1;?></td>
            <td><?php echo $s['Student']['name'];?></td>
            <td><?php echo $s['Student']['npm'];?></td>
            <?php foreach ($romans as $r):?>
                <td class="numeric">
                <?php
                    echo isset($s[$r]['payment_date']) ? $time->format('d/m/Y', $s[$r]['payment_date']) : '';?>
                </td>
                <td class="numeric">
                <?php
                    echo isset($s[$r]['spp']) ? number_format($s[$r]['spp'], 0, '', ',') : '';
                ?>
                </td>
            <?php endforeach;?>
            <td class="nowrap numeric">
            <?php
                foreach ($s['dpp'] as $_d) {
                    echo $time->format('d/m/Y', $_d['payment_date']) . '<br />';
                }
            ?>
            </td>
            <td class="nowrap numeric">
            <?php
                foreach ($s['dpp'] as $_d) {
                    echo number_format($_d['dpp'], 0, '', ',') . '<br />';
                }
            ?>
            </td>
            <td class="nowrap numeric">
            <?php
                echo number_format($s['total'], 0, '', ',');
            ?>
            </td>
            <td class="nowrap numeric">
            <?php
                echo number_format($s['due'], 0, '', ',');
            ?>
            </td>
            <td></td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>
