<?php echo $html->script('student_classes.js?20110129', array('inline' => false));?>
<?php echo $html->css('student_classes.css?20110129', 'stylesheet', array('inline' => false));?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('StudentClass');?>
	<fieldset>
 		<legend>Tambah kelas</legend>
        <table class="input">
            <tr>
                <td class="label-required">Nama Prodi</td>
                <td>
                <?php 
                    echo $form->select('study_program_id', $study_programs, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required'
                        ), true);
                    echo ($form->isFieldError('study_program_id')) ? $form->error('study_program_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nama kelas:</td>
                <td><?php echo $form->input('name', array('div'=>false, 'label' => false, 'maxlength' => 50, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required">Tahun masuk:</td>
                <td>
                <?php
                    echo $form->input('year_started', array(
                        'div'=>false, 'label' => false, 'maxlength' => 4,
                        'class'=>'required year_started', 'type' => 'date',
                        'dateFormat' => 'Y', 'minYear' => $selectMinYear,
                        'maxYear' => $selectMaxYear,
                        'selected' => isset($this->data['StudentClass']['year_started']) ?
                                      $this->data['StudentClass']['year_started'] . '-00-00' : '',
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table id="students" class="students">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Nama mahasiswa</th>
                                <th>NIM</th>
                                <th>Jenis Kelamin</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if ( isset($students) ): ?>
                        <?php
                            foreach ($students as $key => $student):
                                echo '<tr class="row_student" id="r'.$key.'" rel="r'.$key.'">';
                                    echo '<td>' .
                                         '<input type="checkbox" name="data[Student]['.$key.'][Student][id]" class="cb_student" class="inputText" />' .
                                        '</td>';
                                    // nama
                                    echo '<td>';
                                    echo $form->input('name',
                                            array(
                                                'id' => null,
                                                'name' => 'data[Student]['.$key.'][Student][name]',
                                                'class' => 'inputText name',
                                                'div' => false, 'label' => false,
                                                'value' => $student['Student']['name']
                                            )
                                        );
                                    echo '</td>';
                                    // nim
                                    echo '<td>';
                                    echo $form->input('npm', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText npm',
                                            'name' => 'data[Student]['.$key.'][Student][npm]',
                                            'value' => $student['Student']['npm']
                                        ));
                                    echo '</td>';
                                    // gender
                                    echo '<td>';
                                    echo $form->select('gender', array('M' => 'Laki-laki', 'P' => 'Perempuan'), $student['Student']['gender'], array(
                                            'id' => null, 'class' => 'gender',
                                            'name' => 'data[Student]['.$key.'][Student][gender]',
                                            'empty' => false
                                        ));
                                    echo '</td>';
                                echo '</tr>';
                            endforeach;
                        ?>
                        <?php else: ?>
                            <tr class="row_student" id="r0" rel="r0">
                                <td>
                                    <input type="checkbox" name="data[Student][0][Student][id]" class="cb_student" class="inputText" />
                                </td>
                                <td>
                                    <?php
                                        // name
                                        echo $form->input('Student.name',
                                            array(
                                                'id' => null,
                                                'name' => 'data[Student][0][Student][name]',
                                                'class' => 'inputText name',
                                                'div' => false, 'label' => false
                                            )
                                        );
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        // npm
                                        echo $form->input('npm', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText npm',
                                            'name' => 'data[Student][0][Student][npm]'
                                        ));
                                    ?>
                                </td>
                                <td> 
                                    <?php
                                        // gender
                                        echo $form->select(
                                        'gender', array('M' => 'Laki-laki', 'P' => 'Perempuan'), null,
                                        array(
                                            'id' => null,
                                            'class' => 'gender',
                                            'name' => 'data[Student][0][Student][gender]',
                                            'empty' => false
                                        ));
                                    ?>
                                </td>
                            </tr>
                        <?php endif;?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>&nbsp;</th>
                                <th colspan="5">
                                    <span class="up"></span>
                                    <input type="button" name="add_row" id="add_row" value="+ Tambah Mahasiswa" /> &nbsp; atau &nbsp;
                                    <input type="button" name="del_row" id="del_row" value="- Hapus Mahasiswa" />
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
