<?php echo $html->script('jquery.numeric.js?2012011710', array('inline' => true));?>
<?php echo $html->script('student_payment.js', array('inline' => true));?>

<?php echo $html->css('student_payment.css?20120129', 'stylesheet', array('inline' => true));?>
<table class="input">
  <tr>
    <td class="label">Nama Mahasiswa</td>
    <td class="val">
    <?php
        echo $student['Student']['name'];
    ?>
    </td>
    <td class="label" style="padding-left: 40px;">NIM</td>
    <td class="val">
    <?php
        echo $student['Student']['npm'];
    ?>
    </td>
  </tr>
  <tr>
    <td class="label">Kelas</td>
    <td class="val">
    <?php
        echo $student['StudentClass']['name'];
    ?>
    </td>
    <td class="label" style="padding-left: 40px;">Angkatan</td>
    <td class="val">
    <?php
        echo $student['StudentClass']['year_started'];
    ?>
    </td>
  </tr>
</table>

<?php
    echo $form->create('StudentClass', array(
        'id' => 'StudentClass',
        'action' => 'student_payment/' . $student_id
    ));
?>
<table class="payments input" id="payments">
<thead>
  <tr>
    <th>Semester</th>
    <th>DPP</th>
    <th>SPP</th>
    <th>Tgl Bayar</th>
    <th>No bukti (kwitansi)</th>
    <th>Tgl Registrasi</th>
    <th>Terverifikasi</th>
  </tr>
</thead>
<tbody>
<?php foreach ($semester as $s):?>
    <tr class="row_payment">
      <td class="center">
      <?php
          echo $s;
      ?>
      </td>
      <td>
      <?php
          echo $form->input('dpp',
              array(
                  'id' => null,
                  'name' => 'data[StudentPayment][' . $s . '][dpp]',
                  'class' => 'inputText dpp numeric',
                  'div' => false, 'label' => false,
                  'value' => isset($payments[$s]['dpp']) ?
                             $payments[$s]['dpp'] : ''
              )
          );
      ?>
      </td>
      <td>
      <?php
          echo $form->input('spp',
              array(
                  'id' => null,
                  'name' => 'data[StudentPayment][' . $s . '][spp]',
                  'class' => 'inputText spp numeric',
                  'div' => false, 'label' => false,
                  'value' => isset($payments[$s]['spp']) ?
                             $payments[$s]['spp'] : ''
              )
          );
      ?>
      </td>
      <td class="nowrap" nowrap="nowrap">
      <?php
          echo $form->input('payment_date',
              array(
                  'id' => null,
                  'name' => 'data[StudentPayment][' . $s . '][payment_date][day]',
                  'class' => 'payment_date',
                  'type' => 'date', 'dateFormat' => 'D',
                  'div' => false, 'label' => false,
                  'selected' => isset($payments[$s]['payment_date']) ?
                             $payments[$s]['payment_date'] : ''
              )
          ) . '-';
          echo $form->input('payment_date',
              array(
                  'id' => null,
                  'name' => 'data[StudentPayment][' . $s . '][payment_date][month]',
                  'class' => 'payment_date',
                  'type' => 'date', 'dateFormat' => 'M',
                  'div' => false, 'label' => false,
                  'selected' => isset($payments[$s]['payment_date']) ?
                             $payments[$s]['payment_date'] : ''
              )
          ) . '-';
          echo $form->input('payment_date',
              array(
                  'id' => null,
                  'name' => 'data[StudentPayment][' . $s . '][payment_date][year]',
                  'class' => 'payment_date',
                  'type' => 'date', 'dateFormat' => 'Y',
                  'div' => false, 'label' => false,
                  'selected' => isset($payments[$s]['payment_date']) ?
                             $payments[$s]['payment_date'] : ''
              )
          );
      ?>
      </td>
      <td>
      <?php
          echo $form->input('receipt_no',
              array(
                  'id' => null,
                  'name' => 'data[StudentPayment][' . $s . '][receipt_no]',
                  'class' => 'inputText receipt_no',
                  'div' => false, 'label' => false,
                  'value' => isset($payments[$s]['receipt_no']) ?
                             $payments[$s]['receipt_no'] : ''
              )
          );
      ?>
      </td>
      <td class="nowrap" nowrap="nowrap">
      <?php
          echo $form->input('registration_date',
              array(
                  'id' => null,
                  'name' => 'data[StudentPayment][' . $s . '][registration_date][day]',
                  'class' => 'registration_date',
                  'type' => 'date', 'dateFormat' => 'D',
                  'div' => false, 'label' => false,
                  'selected' => isset($payments[$s]['registration_date']) ?
                             $payments[$s]['registration_date'] : ''
              )
          ) . '-';
          echo $form->input('registration_date',
              array(
                  'id' => null,
                  'name' => 'data[StudentPayment][' . $s . '][registration_date][month]',
                  'class' => 'registration_date',
                  'type' => 'date', 'dateFormat' => 'M',
                  'div' => false, 'label' => false,
                  'selected' => isset($payments[$s]['registration_date']) ?
                             $payments[$s]['registration_date'] : ''
              )
          ) . '-';
          echo $form->input('registration_date',
              array(
                  'id' => null,
                  'name' => 'data[StudentPayment][' . $s . '][registration_date][year]',
                  'class' => 'registration_date',
                  'type' => 'date', 'dateFormat' => 'Y',
                  'div' => false, 'label' => false,
                  'selected' => isset($payments[$s]['registration_date']) ?
                             $payments[$s]['registration_date'] : ''
              )
          );
      ?>
      </td>
      <td class="center">
          <?php if (isset($payments[$s]['verified']) && $payments[$s]['verified']): ?>
              <input type="checkbox" class="cb_payment" checked="checked" name="data[StudentPayment][<?php echo $s;?>][verified]" value="1" />
          <?php else: ?>
              <input type="checkbox" class="cb_payment" name="data[StudentPayment][<?php echo $s;?>][verified]" value="1" />
          <?php endif;?>
      </td>
    </tr>
<?php endforeach;?>
</tbody>
</table>
<p style="margin-top: 20px;">
<?php
    echo $form->submit('Simpan', array('div'=>false));
?>
</p>
