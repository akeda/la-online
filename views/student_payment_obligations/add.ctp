<?php echo $html->script('jquery.numeric.js?2012011710', array('inline' => true));?>

<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('StudentPaymentObligation');?>
	<fieldset>
 		<legend>Nilai besaran SPP dan DPP per angkatan</legend>
        <table class="input">
            <tr>
                <td class="label-required">Angkatan / Tahun Masuk:</td>
                <td>
                <?php
                    echo $form->input('year_started', array(
                        'div' => false, 'label' => false, 'maxlength' => 100,
                        'name' => 'data[StudentPaymentObligation][year_started]',
                        'class' => 'required year_started', 'type' => 'date',
                        'dateFormat' => 'Y', 'minYear' => $selectMinYear,
                        'selected' => isset($this->data['StudentPaymentObligation']['year_started']) ?
                                      $this->data['StudentPaymentObligation']['year_started'] . '-00-00' : '',
                        'maxYear' => $selectMaxYear
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nilai SPP (per semester):</td>
                <td>
                <?php
                    echo $form->input('spp', array(
                        'div'=>false, 'label' => false,
                        'class'=>'required numeric'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nilai DPP:</td>
                <td>
                <?php
                    echo $form->input('dpp', array(
                        'div'=>false, 'label' => false,
                        'class'=>'required numeric'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>

<script type="text/javascript">
$(function() {
  $('.numeric').numeric().trigger('blur');
  $('#StudentPaymentObligationAddForm').submit(function() {
    $('.numeric').each(function() {
      this.value = this.value.replace(/,/gi, '');
    });
  });
});
</script>
