<?php echo $html->script('jquery.numeric.js?2012011710', array('inline' => true));?>

<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('StudentPaymentObligation');?>
	<fieldset>
 		<legend>Edit besar SPP dan DPP angkatan</legend>
        <table class="input">
            <tr>
                <td class="label-required">Angkatan / Tahun Masuk:</td>
                <td>
                <?php
                    echo $form->input('year_started', array(
                        'div' => false, 'label' => false, 'maxlength' => 100,
                        'name' => 'data[StudentPaymentObligation][year_started]',
                        'class' => 'required year_started', 'type' => 'date',
                        'dateFormat' => 'Y', 'minYear' => $selectMinYear,
                        'selected' => $this->data['StudentPaymentObligation']['year_started'] .
                                      '-00-00',
                        'maxYear' => $selectMaxYear
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nilai SPP (per semester):</td>
                <td>
                <?php
                    echo $form->input('spp', array(
                        'div'=>false, 'label' => false,
                        'class'=>'required numeric'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nilai DPP:</td>
                <td>
                <?php
                    echo $form->input('dpp', array(
                        'div'=>false, 'label' => false,
                        'class'=>'required numeric'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Save', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['StudentPaymentObligation']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['StudentPaymentObligation']['year_started'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>

<script type="text/javascript">
$(function() {
  $('.numeric').numeric().trigger('blur');
  $('#StudentPaymentObligationEditForm').submit(function() {
    $('.numeric').each(function() {
      this.value = this.value.replace(/,/gi, '');
    });
  });
});
</script>
