<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                'year_started' => array(
                    'title' => 'Angkatan', 
                    'sortable' => true
                ),
                'spp' => array(
                    'title' => 'SPP',
                    'sortable' => false
                ),
                'dpp' => array(
                    'title' => 'SPP',
                    'sortable' => false
                ),
              ),
              "editable"  => "year_started",
              "displayedAs" => array(
                'spp' => 'money',
                'dpp' => 'money',
              )
        ));
?>
</div>
