<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('StudyProgram');?>
	<fieldset>
 		<legend>Tambah Prodi</legend>
        <table class="input">
            <tr>
                <td class="label-required">Nama prodi:</td>
                <td><?php echo $form->input('name', array('div'=>false, 'label' => false, 'maxlength' => 50, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
