<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array(
            "fields" => array(
                "dupak_date" => array(
                    'title' => 'Tanggal Penyerahan',
                    'sortable' => true
                ),
                "created_by" => array(
                    'title' => __('Name Dosen', true),
                    'sortable' => true
                ),
                "created" => array(
                    'title' => __('Waktu Input', true),
                    'sortable' => true
                )
            ),
            "editable"  => "dupak_date",
            "assoc" => array(
                'created_by' => array(
                    'model' => 'CreatedBy',
                    'field' => 'name'
                )
            ),
            'displayedAs' => array(
                'dupak_date' => 'date',
                'created' => 'datetime'
            )
        ));
?>
</div>
