<?php echo $html->css('dupaks');?>
<?php echo $javascript->link('jquery.numeric', false);?>

<script type="text/javascript">
    var chkTransURL = '<?php echo $ajaxURL;?>';
    var onAdd = true;
</script>
<?php echo $javascript->link('dupaks');?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Dupak');?>
	<fieldset>
 		<legend><?php __('Input DUPAK');?></legend>
        <table class="input">
            <tr>
                <td class="label-required">Tanggal DUPAK</td>
                <td>
                <?php
                    echo $form->input('dupak_date', array(
                        'div'=>false, 'label'=>false, 'class'=>'dupak_date required',
                        'type' => 'date'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="section section_1 separate">
                        <thead>
                            <?php /* Keterangan Perorangan */?>
                            <tr>
                                <th><h3>I</h3></th>
                                <th colspan="15" class="center"><h3>KETERANGAN PERORANGAN</h3></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="no">1</td>
                                <td colspan="2">Nama</td>
                                <td colspan="6"><?php echo $profile['name'];?></td>
                            </tr>
                            <tr>
                                <td class="no">2</td>
                                <td colspan="2">NIP</td>
                                <td colspan="6"><?php echo $profile['nip'];?></td>
                            </tr>
                            <tr>
                                <td class="no">3</td>
                                <td colspan="2">Nomor seri Karpeg</td>
                                <td colspan="6">
                                <?php
                                    echo $form->input('profile_karpeg', array(
                                        'class' => 'required inputText', 'div' => false, 'label' => false
                                    ));
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">4</td>
                                <td colspan="2">Tempat dan tanggal lahir</td>
                                <td colspan="6">
                                <?php
                                    echo $form->input('profile_pob', array(
                                        'class' => 'required inputText', 'div' => false, 'label' => false,
                                        'style' => 'margin-bottom: 2px;'
                                    ));
                                    echo '<br />';
                                    echo $form->input('profile_dob', array(
                                        'class' => 'required', 'div' => false, 'label' => false,
                                        'minYear' => '1940', 'maxYear' => date('Y')-10
                                    ));
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">5</td>
                                <td colspan="2">Jenis Kelamin</td>
                                <td colspan="6">
                                <?php
                                    echo $form->radio('profile_gender', array('M' => 'Laki-laki &nbsp; ', 'F' => 'Perempuan'),
                                        array(
                                            'class' => 'required', 'div' => false, 'label' => false,
                                            'value' => 'F', 'type' => 'radio', 'legend' => false
                                        )
                                    );
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">6</td>
                                <td colspan="2">Pendidikan Tertinggi</td>
                                <td colspan="6">
                                <?php
                                    echo $form->input('profile_education', array(
                                        'class' => 'required inputText', 'div' => false, 'label' => false
                                    ));
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">7</td>
                                <td colspan="2">Pangkat dan golongan ruang/tmt</td>
                                <td colspan="6">
                                <?php
                                    echo $form->input('profile_category', array(
                                        'class' => 'required inputText', 'div' => false, 'label' => false
                                    ));
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">8</td>
                                <td colspan="2">Jabatan fungsional dosen/tmt</td>
                                <td colspan="6">
                                <?php
                                    echo $form->input('profile_position', array(
                                        'class' => 'required inputText', 'div' => false, 'label' => false
                                    ));
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">9</td>
                                <td colspan="2">Fakultas/jurusan</td>
                                <td colspan="6">
                                <?php
                                    echo $form->input('profile_faculty', array(
                                        'class' => 'required inputText', 'div' => false, 'label' => false
                                    ));
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">10</td>
                                <td rowspan="2">Masa Kerja</td>
                                <td>Dalam jabatan terakhir</td>
                                <td colspan="6">
                                <?php
                                    echo $form->input('profile_duration_cat', array(
                                        'class' => 'required inputText', 'div' => false, 'label' => false
                                    ));
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">11</td>
                                <td>Dalam pangkat terakhir</td>
                                <td colspan="6">
                                <?php
                                    echo $form->input('profile_duration_pos', array(
                                        'class' => 'required inputText', 'div' => false, 'label' => false
                                    ));
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no last">12</td>
                                <td colspan="2">Unit Kerja</td>
                                <td colspan="6">
                                <?php
                                    echo $form->input('profile_unit_code', array(
                                        'class' => 'required inputText', 'div' => false, 'label' => false
                                    ));
                                ?>
                                </td>
                            </tr>
                            <?php /* unsur yang dinilai */?>
                            <tr>
                                <td><h3>II</h3></td>
                                <td colspan="2"><h3>UNSUR YANG DINILAI</h3></td>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr>
                                <td rowspan="3">&nbsp;</td>
                                <td rowspan="3" colspan="2">&nbsp;</td>
                                <td colspan="6" class="center"><h3>Angka Kredit Menurut</h3></td>
                            </tr>
                            <tr>
                                <td colspan="3" class="center">Perguruan tinggi/Kopertis pengusul</td>
                                <td colspan="3" class="center">Tim penilai</td>
                            </tr>
                            <tr>
                                <td>Lama</td>
                                <td>Baru</td>
                                <td>Jumlah</td>
                                <td>Lama</td>
                                <td>Baru</td>
                                <td>Jumlah</td>
                            </tr>
                            <?php /* unsur utama */?>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2"><h3>I. &nbsp; UNSUR UTAMA</h3></td>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2"><h4>A. &nbsp; PENDIDIKAN</h4></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1">a. Mengikuti pendidikan sekolah dan memperoleh gelar/sebutan/ijazah/akta</td>
                                <?php // s_2_A_a_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' .
                                                $form->input('s_2_A_a_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1">b. Mengikuti pendidikan sekolah dan memperoleh gelar/sebutan/ijazah/akta
                                tambahan yang setingkat atau lebih tinggi di luar bidang ilmunya.
                                </td>
                                <?php // s_2_A_b_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_A_b_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1">c. Mengikuti pendidikan sekolah dan pelatihan fungsional
                                dosen dan memperoleh Surat Tanda Tamat Pendidikan dan Pelatihan (STTPL)
                                </td>
                                <?php // s_2_A_c_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_A_c_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2 center"><strong>Sub Jumlah</strong></td>
                                <?php // s_2_A_sub_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_A_sub_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2"><h4>B. &nbsp; TRI DHARMA PERGURUAN TINGGI</h4></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1"><h4>a. &nbsp; MELAKSANAKAN PENDIDIKAN DAN PENGAJARAN</h4></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">1. Melaksanakan perkuliahan/tutorial dan membimbing,
                                menguji serta menyelenggarakan pendidikan di laboratorium, praktek keguruan, bengkel/studio/kebun
                                percobaan/teknologi pengajaran dan lapangan
                                </td>
                                <?php // s_2_B_a_1_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_a_1_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">2. Membimbing seminar mahasiswa 
                                </td>
                                <?php // s_2_B_a_2_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_a_2_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">3. Membimbing Kuliah Kerja Nyata (KKN), Praktek
                                Kerja Nyata (PKN), dan Praktek Kerja Lapangan (PKL)
                                </td>
                                <?php // s_2_B_a_3_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_a_3_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">4. Membimbing dan ikut membimbing dalam menghasilkan
                                laporan akhir studi/skripsi/thesis/disertasi
                                </td>
                                <?php // s_2_B_a_4_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_a_4_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">5. Bertugas sebagai penguji pada ujian akhir
                                </td>
                                <?php // s_2_B_a_5_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_a_5_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">6. Membina kegiatan mahasiswa di bidang akademik
                                dan kemahasiswaan
                                </td>
                                <?php // s_2_B_a_6_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_a_6_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">7. Mengembangkan program kuliah
                                </td>
                                <?php // s_2_B_a_7_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_a_7_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">8. Mengembangkan bahan pengajaran
                                </td>
                                <?php // s_2_B_a_8_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_a_8_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">9. Menyampaikan orasi ilmiah
                                </td>
                                <?php // s_2_B_a_9_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_a_9_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">10. Menduduki jabatan pimpinan perguruan tinggi
                                </td>
                                <?php // s_2_B_a_10_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_a_10_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">11. Membimbing dosen yang lebih rendah jabatan
                                fungsionalnya
                                </td>
                                <?php // s_2_B_a_11_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_a_11_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">12. Melaksanakan kegiatan datasering dan pencangkokan dosen
                                </td>
                                <?php // s_2_B_a_12_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_a_12_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2 center"><strong>Sub Jumlah</strong></td>
                                <?php // s_2_B_a_sub_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_a_sub_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1"><h4>b. &nbsp; MELAKSANAKAN PENELITIAN</h4></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">1. Menghasilkan karya ilmiah
                                </td>
                                <?php // s_2_B_b_1_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_b_1_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">2. Menerjemahkan/menyadur buku ilmiah
                                </td>
                                <?php // s_2_B_b_2_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_b_2_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">3. Mengedit/menyunting karya ilmiah
                                </td>
                                <?php // s_2_B_b_3_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_b_3_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">4. Membuat rancangan dan karya teknologi
                                yang dipatenkan
                                </td>
                                <?php // s_2_B_b_4_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_b_4_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">5. Membuat rancangan dan karya teknologi,
                                rancangan dan karya seni monumental/seni pertunjukan/karya sastra
                                </td>
                                <?php // s_2_B_b_5_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_b_5_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2 center"><strong>Sub Jumlah</strong></td>
                                <?php // s_2_B_b_sub_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_b_sub_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1"><h4>c. &nbsp; MELAKSANAKAN PENGABDIAN KEPADA MASYARAKAT</h4></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">1. Menduduki jabatan pimpinan pada lembaga
                                pemerintahan/pejabat negara yang harus dibebaskan dari jabatan organiknya
                                </td>
                                <?php // s_2_B_c_1_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_c_1_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">2. Melaksanakan pengembangan hasil pendidikan
                                dan penelitian yang dapat dimanfaatkan oleh masyarakat
                                </td>
                                <?php // s_2_B_c_2_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_c_2_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">3. Memberi latihan/penyuluhan/penataran/ceramah pada
                                masyarakat
                                </td>
                                <?php // s_2_B_c_3_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_c_3_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">4. Memberi pelayanan kepada masyarakat atau
                                kegiatan lain yang menunjang pelaksanaan tugas umum pemerintahan dan pembangungan
                                </td>
                                <?php // s_2_B_c_4_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_c_4_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">5. Membuat/menulis karya pengabdian kepada masyarakat
                                yang tidak dipublikasikan
                                </td>
                                <?php // s_2_B_c_5_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_c_5_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2 center"><strong>Sub Jumlah</strong></td>
                                <?php // s_2_B_c_sub_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_B_c_sub_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <?php /* unsur penunjang */?>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2"><h3>II. &nbsp; UNSUR PENUNJANG</h3></td>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1"><h4>&nbsp; PENUNJANG TUGAS POKOK DOSEN</h4></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">1. Menjadi anggota dalam suatu panitia/badan pada
                                perguruan tinggi
                                </td>
                                <?php // s_2_2_1_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_2_1_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5,
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">2. Menjadi anggota panitia/badan pada
                                lembaga pemerintahan
                                </td>
                                <?php // s_2_2_2_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_2_2_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5,
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">3. Menjadi anggota organisasi profesi
                                </td>
                                <?php // s_2_2_3_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_2_3_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5,
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">4. Mewakili perguruan tinggi/lembaga pemerintahan
                                duduk dalam panitia antar lembaga
                                </td>
                                <?php // s_2_2_4_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_2_4_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5,
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">5. Menjadi anggota delegasi nasional ke
                                pertemuan internasional
                                </td>
                                <?php // s_2_2_5_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_2_5_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5,
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">6. Berperan serta aktif dalam pertemuan ilmiah
                                </td>
                                <?php // s_2_2_6_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_2_6_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5,
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">7. Mendapat tanda jasa/penghargaan
                                </td>
                                <?php // s_2_2_7_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_2_7_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5,
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">8. Menulis buku pelajaran SLTA ke bawah yang
                                diterbitkan dan diedarkan secara nasional
                                </td>
                                <?php // s_2_2_8_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_2_8_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5,
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">9. Mempunyai prestasi di bidang olahraga/humanoria.
                                </td>
                                <?php // s_2_2_9_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_2_9_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5,
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2 center"><strong>Sub Jumlah</strong></td>
                                <?php // s_2_2_sub_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('s_2_2_sub_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td colspan="3" class="center"><h3>JUMLAH</h3></td>
                                <?php // sub_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'.
                                                $form->input('sub_' . $i, array(
                                                    'div' => false, 'label' => false, 'maxlength' => 5, 
                                                    'size' => 5, 'class' => 'numeric'
                                                )) .
                                             '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td><h3>III</h3></td>
                                <td colspan="15"><h3>BAHAN YANG DINILAI</h3></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="15">
                                    <div class="prepend-1">
                                        <div style="padding-left: 20px">
                                            <span style="width: 150px; display: inline-block; padding: 2px;">Nama</span>:
                                        </div>
                                        <div style="padding-left: 20px">
                                            <span style="width: 150px; display: inline-block; padding: 2px;">NIP</span>:
                                        </div>
                                        <div style="padding-left: 20px">
                                            <span style="width: 300px; display: inline-block; padding: 2px;">Jurusan/program studi</span>
                                        </div>
                                    </div>
                                    
                                    <div style="float: right; margin-right: 100px; padding-bottom: 4px;">
                                        <p style="margin-bottom: 80px;">
                                        Rektor/Ketua ST/Direktur<br />
                                        Koordinator Kopertis Wilayah IV
                                        </p>
                                        .........................................................<br />
                                        NIP
                                    </div>
                                    <br style="clear: both;" />
                                </td>
                            </tr>
                            <tr>
                                <td><h3>IV</h3></td>
                                <td colspan="15"><h3>PENDAPAT TIM PENILAI ANGKA KREDIT JABATAN DOSEN</h3></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="15">
                                    <div class="prepend-1">
                                        <div style="padding-left: 20px">
                                            <span style="display: inline-block; padding: 2px;">Telah memenuhi syarat angka kredit untuk kenaikan jabatan/pangkat</span>
                                        </div>
                                        <div style="padding-left: 20px">
                                            <span style="width: 150px; display: inline-block; padding:  10px 2px 5px 2px;"><strong>menjadi</strong></span>: &nbsp; &nbsp; <strong>*)</strong>
                                        </div>
                                        <div style="padding-left: 20px">
                                            <span style="width: 150px; display: inline-block; padding: 10px 2px 5px 2px;"><strong>Tmt.</strong></span>:
                                        </div>
                                    </div>
                                    
                                    <div style="float: right; margin-right: 100px; padding-bottom: 80px;">
                                        <p style="margin-bottom: 10px">
                                        Jakarta,<br />
                                        </p>
                                        <strong>Tim Penilai Pusat</strong><br />
                                        <strong>Ketua</strong>
                                    </div>
                                    <br style="clear: both;" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Simpan', array('div'=>false, 'id' => 'add')) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
