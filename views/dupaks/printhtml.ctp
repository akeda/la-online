<?php if ($show_form):?>
<fieldset>
<legend>DAFTAR USUL PENETAPAN ANGKA KREDIT (DUPAK) JABATAN FUNGSIONAL DOSEN</legend>
<?php echo $form->create('Dupak');?>
    <table class="input">
        <tr>
            <td>Tahun DUPAK</td>
            <td>
                <?php 
                    echo $form->year('y', '2008', date('Y'), date('Y'), array(
                        'empty' => false
                    ));
                ?>
            </td>
        </tr>
        <tr>
            <td>Pengguna</td>
            <td>
                <?php 
                    echo $form->select('user', $users, null, array(
                        'empty' => false
                    ));
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Cetak" />
            </td>
        </tr>
    </table>
</form>
</fieldset>
<?php else:?>
<?php echo $html->css('dupaks_print');?>
<h1>DAFTAR USUL PENETAPAN ANGKA KREDIT (DUPAK)<br />JABATAN FUNGSIONAL DOSEN</h1>
<br />
<center>
        <table class="noborder">
            <tr>
                <td class="label-required">Tanggal Penilaian</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="section section_1">
                        <tbody>
                            <?php /* Keterangan Perorangan */?>
                            <tr>
                                <td><h3>I</h3></td>
                                <td colspan="15" class="center"><h3>KETERANGAN PERORANGAN</h3></td>
                            </tr>
                            <tr>
                                <td class="no">1</td>
                                <td colspan="2">Nama</td>
                                <td colspan="6"><?php echo $r['CreatedBy']['name'];?></td>
                            </tr>
                            <tr>
                                <td class="no">2</td>
                                <td colspan="2">NIP</td>
                                <td colspan="6"><?php echo $r['CreatedBy']['nip'];?></td>
                            </tr>
                            <tr>
                                <td class="no">3</td>
                                <td colspan="2">Nomor seri Karpeg</td>
                                <td colspan="6">
                                <?php echo $r['Dupak']['profile_karpeg'];?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">4</td>
                                <td colspan="2">Tempat dan tanggal lahir</td>
                                <td colspan="6">
                                <?php
                                    echo $r['Dupak']['profile_pob'] . ', ' . $r['Dupak']['profile_dob'];
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">5</td>
                                <td colspan="2">Jenis Kelamin</td>
                                <td colspan="6">
                                <?php
                                    if ( $r['Dupak']['profile_gender'] == 'P' ) {
                                        echo "Perempuan";
                                    } else {
                                        echo "Laki-laki";
                                    }
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">6</td>
                                <td colspan="2">Pendidikan Tertinggi</td>
                                <td colspan="6">
                                <?php
                                    echo $r['Dupak']['profile_education'];
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">7</td>
                                <td colspan="2">Pangkat dan golongan ruang/tmt</td>
                                <td colspan="6">
                                <?php
                                    echo $r['Dupak']['profile_category'];
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">8</td>
                                <td colspan="2">Jabatan fungsional dosen/tmt</td>
                                <td colspan="6">
                                <?php
                                    echo $r['Dupak']['profile_position'];
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">9</td>
                                <td colspan="2">Fakultas/jurusan</td>
                                <td colspan="6">
                                <?php
                                    echo $r['Dupak']['profile_faculty'];
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">10</td>
                                <td rowspan="2">Masa Kerja</td>
                                <td>Dalam jabatan terakhir</td>
                                <td colspan="6">
                                <?php
                                    echo $r['Dupak']['profile_duration_cat'];
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no">11</td>
                                <td>Dalam pangkat terakhir</td>
                                <td colspan="6">
                                <?php
                                    echo $r['Dupak']['profile_duration_pos'];
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="no last">12</td>
                                <td colspan="2">Unit Kerja</td>
                                <td colspan="6">
                                <?php
                                    echo $r['Dupak']['profile_unit_code'];
                                ?>
                                </td>
                            </tr>
                            <?php /* unsur yang dinilai */?>
                            <tr>
                                <td><h3>II</h3></td>
                                <td colspan="2"><h3>UNSUR YANG DINILAI</h3></td>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr>
                                <td rowspan="3">&nbsp;</td>
                                <td rowspan="3" colspan="2">&nbsp;</td>
                                <td colspan="6" class="center"><h3>Angka Kredit Menurut</h3></td>
                            </tr>
                            <tr>
                                <td colspan="3" class="center">Perguruan tinggi/Kopertis pengusul</td>
                                <td colspan="3" class="center">Tim penilai</td>
                            </tr>
                            <tr>
                                <td>Lama</td>
                                <td>Baru</td>
                                <td>Jumlah</td>
                                <td>Lama</td>
                                <td>Baru</td>
                                <td>Jumlah</td>
                            </tr>
                            <?php /* unsur utama */?>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2"><h3>I. &nbsp; UNSUR UTAMA</h3></td>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2"><h4>A. &nbsp; PENDIDIKAN</h4></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1">a. Mengikuti pendidikan sekolah dan memperoleh
                                gelar/sebutan/ijazah/akta</td>
                                <?php // s_2_A_a_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_A_a_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1">b. Mengikuti pendidikan sekolah dan memperoleh gelar/sebutan/ijazah/akta
                                tambahan yang setingkat atau lebih tinggi di luar bidang ilmunya.
                                </td>
                                <?php // s_2_A_b_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_A_b_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1">c. Mengikuti pendidikan sekolah dan pelatihan fungsional
                                dosen dan memperoleh Surat Tanda Tamat Pendidikan dan Pelatihan (STTPL)
                                </td>
                                <?php // s_2_A_c_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_A_c_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2 center"><strong>Sub Jumlah</strong></td>
                                <?php
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_A_sub_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2"><h4>B. &nbsp; TRI DHARMA PERGURUAN TINGGI</h4></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1"><h4>a. &nbsp; MELAKSANAKAN PENDIDIKAN DAN PENGAJARAN</h4></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">1. Melaksanakan perkuliahan/tutorial dan membimbing,
                                menguji serta menyelenggarakan pendidikan di laboratorium, praktek keguruan, bengkel/studio/kebun
                                percobaan/teknologi pengajaran dan lapangan
                                </td>
                                <?php // s_2_B_a_1_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_a_1_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">2. Membimbing seminar mahasiswa 
                                </td>
                                <?php // s_2_B_a_2_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_a_2_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">3. Membimbing Kuliah Kerja Nyata (KKN), Praktek
                                Kerja Nyata (PKN), dan Praktek Kerja Lapangan (PKL)
                                </td>
                                <?php // s_2_B_a_3_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_a_3_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">4. Membimbing dan ikut membimbing dalam menghasilkan
                                laporan akhir studi/skripsi/thesis/disertasi
                                </td>
                                <?php // s_2_B_a_4_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_a_4_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">5. Bertugas sebagai penguji pada ujian akhir
                                </td>
                                <?php // s_2_B_a_5_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_a_5_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">6. Membina kegiatan mahasiswa di bidang akademik
                                dan kemahasiswaan
                                </td>
                                <?php // s_2_B_a_6_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_a_6_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">7. Mengembangkan program kuliah
                                </td>
                                <?php // s_2_B_a_7_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_a_7_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">8. Mengembangkan bahan pengajaran
                                </td>
                                <?php // s_2_B_a_8_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_a_8_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">9. Menyampaikan orasi ilmiah
                                </td>
                                <?php // s_2_B_a_9_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_a_9_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">10. Menduduki jabatan pimpinan perguruan tinggi
                                </td>
                                <?php // s_2_B_a_10_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_a_10_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">11. Membimbing dosen yang lebih rendah jabatan
                                fungsionalnya
                                </td>
                                <?php // s_2_B_a_11_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_a_11_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">12. Melaksanakan kegiatan datasering dan pencangkokan dosen
                                </td>
                                <?php // s_2_B_a_12_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_a_12_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2 center"><strong>Sub Jumlah</strong></td>
                                <?php
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_a_sub_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1"><h4>b. &nbsp; MELAKSANAKAN PENELITIAN</h4></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">1. Menghasilkan karya ilmiah
                                </td>
                                <?php // s_2_B_b_1_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_b_1_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">2. Menerjemahkan/menyadur buku ilmiah
                                </td>
                                <?php // s_2_B_b_2_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_b_2_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">3. Mengedit/menyunting karya ilmiah
                                </td>
                                <?php // s_2_B_b_3_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_b_3_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">4. Membuat rancangan dan karya teknologi
                                yang dipatenkan
                                </td>
                                <?php // s_2_B_b_4_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_b_4_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">5. Membuat rancangan dan karya teknologi,
                                rancangan dan karya seni monumental/seni pertunjukan/karya sastra
                                </td>
                                <?php // s_2_B_b_5_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_b_5_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2 center"><strong>Sub Jumlah</strong></td>
                                <?php
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_b_sub' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1"><h4>c. &nbsp; MELAKSANAKAN PENGABDIAN KEPADA MASYARAKAT</h4></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">1. Menduduki jabatan pimpinan pada lembaga
                                pemerintahan/pejabat negara yang harus dibebaskan dari jabatan organiknya
                                </td>
                                <?php // s_2_B_c_1_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_c_1_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">2. Melaksanakan pengembangan hasil pendidikan
                                dan penelitian yang dapat dimanfaatkan oleh masyarakat
                                </td>
                                <?php // s_2_B_c_2_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_c_2_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">3. Memberi latihan/penyuluhan/penataran/ceramah pada
                                masyarakat
                                </td>
                                <?php // s_2_B_c_3_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">'. $r['Dupak']['s_2_B_c_3_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">4. Memberi pelayanan kepada masyarakat atau
                                kegiatan lain yang menunjang pelaksanaan tugas umum pemerintahan dan pembangungan
                                </td>
                                <?php // s_2_B_c_4_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_c_4_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">5. Membuat/menulis karya pengabdian kepada masyarakat
                                yang tidak dipublikasikan
                                </td>
                                <?php // s_2_B_c_5_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_c_5_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2 center"><strong>Sub Jumlah</strong></td>
                                <?php
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_B_c_sub_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <?php /* unsur penunjang */?>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2"><h3>II. &nbsp; UNSUR PENUNJANG</h3></td>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-1"><h4>&nbsp; PENUNJANG TUGAS POKOK DOSEN</h4></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">1. Menjadi anggota dalam suatu panitia/badan pada
                                perguruan tinggi
                                </td>
                                <?php // s_2_2_1_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_2_1_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">2. Menjadi anggota panitia/badan pada
                                lembaga pemerintahan
                                </td>
                                <?php // s_2_2_2_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_2_2_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">3. Menjadi anggota organisasi profesi
                                </td>
                                <?php // s_2_2_3_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_2_3_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">4. Mewakili perguruan tinggi/lembaga pemerintahan
                                duduk dalam panitia antar lembaga
                                </td>
                                <?php // s_2_2_4_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_2_4_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">5. Menjadi anggota delegasi nasional ke
                                pertemuan internasional
                                </td>
                                <?php // s_2_2_5_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_2_5_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">6. Berperan serta aktif dalam pertemuan ilmiah
                                </td>
                                <?php // s_2_2_6_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_2_6_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">7. Mendapat tanda jasa/penghargaan
                                </td>
                                <?php // s_2_2_7_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_2_7_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">8. Menulis buku pelajaran SLTA ke bawah yang
                                diterbitkan dan diedarkan secara nasional
                                </td>
                                <?php // s_2_2_8_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_2_8_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2">9. Mempunyai prestasi di bidang olahraga/humanoria.
                                </td>
                                <?php // s_2_2_9_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_2_9_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="prepend-2 center"><strong>Sub Jumlah</strong></td>
                                <?php
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['s_2_2_sub_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td colspan="3" class="center"><h3>JUMLAH</h3></td>
                                <?php // sub_(1-6)
                                    for ($i = 1; $i <= 6; $i++) {
                                        echo '<td class="center">' . $r['Dupak']['sub_' . $i] . '</td>';
                                    }
                                ?>
                            </tr>
                            <tr>
                                <td><h3>III</h3></td>
                                <td colspan="15"><h3>BAHAN YANG DINILAI</h3></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="15">
                                    <div class="prepend-1">
                                        <div style="padding-left: 20px">
                                            <span style="width: 150px; display: inline-block; padding: 2px;">Nama</span>:
                                        </div>
                                        <div style="padding-left: 20px">
                                            <span style="width: 150px; display: inline-block; padding: 2px;">NIP</span>:
                                        </div>
                                        <div style="padding-left: 20px">
                                            <span style="width: 300px; display: inline-block; padding: 2px;">Jurusan/program studi</span>
                                        </div>
                                    </div>
                                    
                                    <div style="float: right; margin-right: 100px; padding-bottom: 4px;">
                                        <p style="margin-bottom: 80px;">
                                        Rektor/Ketua ST/Direktur<br />
                                        Koordinator Kopertis Wilayah IV
                                        </p>
                                        .........................................................<br />
                                        NIP
                                    </div>
                                    <br style="clear: both;" />
                                </td>
                            </tr>
                            <tr>
                                <td><h3>IV</h3></td>
                                <td colspan="15"><h3>PENDAPAT TIM PENILAI ANGKA KREDIT JABATAN DOSEN</h3></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="15">
                                    <div class="prepend-1">
                                        <div style="padding-left: 20px">
                                            <span style="display: inline-block; padding: 2px;">Telah memenuhi syarat angka kredit untuk kenaikan jabatan/pangkat</span>
                                        </div>
                                        <div style="padding-left: 20px">
                                            <span style="width: 150px; display: inline-block; padding:  10px 2px 5px 2px;"><strong>menjadi</strong></span>: &nbsp; &nbsp; <strong>*)</strong>
                                        </div>
                                        <div style="padding-left: 20px">
                                            <span style="width: 150px; display: inline-block; padding: 10px 2px 5px 2px;"><strong>Tmt.</strong></span>:
                                        </div>
                                    </div>
                                    
                                    <div style="float: right; margin-right: 100px; padding-bottom: 80px;">
                                        <p style="margin-bottom: 10px">
                                        Jakarta,<br />
                                        </p>
                                        <strong>Tim Penilai Pusat</strong><br />
                                        <strong>Ketua</strong>
                                    </div>
                                    <br style="clear: both;" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p><br />
                    Keterangan : <br />
                    *) Diisi sesuai usulannya<br />
                    Misal : (Lektor Kepala; atau Pembina Tk.I (IV/b) atau Pembina Utama Muda (IV/c), Guru Besar 
                    </p>
                </td>
            </tr>
        </table>
</center>
<?php endif;?>
