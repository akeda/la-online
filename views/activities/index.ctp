<?php echo $html->css('activities_index', 'stylesheet', array('inline' => false));?>
<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array(
            "fields" => array(
                'position' => array(
                    'title' => __('Jabatan yang melakukan kegiatan', true),
                    'sortable' => false
                ),
                'activities' => array(
                    'title' => __('Nama Kegiatan', true),
                    'sortable' => true,
                    'wrapBefore' => '<div class="activities">',
                    'wrapAfter' => '</div>'
                )
            ),
            "if" => array(
                'activities' => array(
                    'operator' => 'hasManyImplodeNumbered',
                    'model' => 'Act',
                    'field' => 'name',
                    'separator' => ', ',
                    'link' => array(
                        'controller' => 'activities',
                        'action' => 'edit',
                        'params' => array(
                            'field' => 'id'
                        )
                    )
                )
            )
        ));
?>
</div>
