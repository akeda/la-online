<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Activity');?>
	<fieldset>
 		<legend><?php __('Edit Kegiatan');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Nama Kegiatan', true);?></td>
                <td>
                <?php
                    echo $form->input('name', array(
                        'div'=>false, 'label'=>false, 'class'=>'required',
                        'type' => 'textarea'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Kegiatan ini untuk jabatan', true);?></td>
                <td><?php echo $form->select('position_id', $positions, null, array('empty' => false));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Kegiatan khusus pengajaran', true);?><br />
                <span class="label">Centang jika kegiatan belajar mengajar</span>
                </td>
                <td>
                    <?php echo $form->input('special', array('type' => 'checkbox', 'div' => false, 'label' => false));?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Update', true), array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link( 
                        __('Delete', true), 
                        array('action'=>'delete', $this->data['Activity']['id']),
                        array('class'=>'del'),
                        sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['Activity']['name'])
                    ) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
