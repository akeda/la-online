<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array(
            "fields" => array(
                "name" => array(
                    'title' => __('Nama Jabatan', true),
                    'sortable' => true
                ),
                'verfied_positions' => array(
                    'title' => __('Posisi yang diverifikasi', true),
                    'sortable' => false
                ),
                'special' => array(
                    'title' => 'Approval',
                    'sortable' => false
                )
            ),
            "editable"  => "name",
            'replacement' => array(
                'special' => array(
                    0 => array(
                        'replaced' => '<strong class="red">Bukan</strong>'
                    ),
                    1 => array(
                        'replaced' => '<strong class="green">Ya</strong>'
                    )
                )
            )
        ));
?>
</div>
