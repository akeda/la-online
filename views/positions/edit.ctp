<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Position');?>
	<fieldset>
 		<legend><?php __('Edit Jabatan');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Name', true);?></td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <?php if (!empty($positions)): ?>
            <tr>
                <td class="label-required"><?php echo __('Diverifikasi oleh', true);?></td>
                <td><?php echo $form->select('parent_id', $positions, null, array('escape' => false));?></td>
            </tr>
            <?php endif;?>
            <tr>
                <td class="label-required"><?php echo __('Pemverifikasi Ka Prodi', true);?></td>
                <td>
                <?php
                    echo $form->input('special', array(
                        'type' => 'checkbox',
                        'div'=>false, 'label'=>false, 'class'=>'required'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Update', true), array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link( 
                        __('Delete', true), 
                        array('action'=>'delete', $this->data['Position']['id']),
                        array('class'=>'del'),
                        sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['Position']['name'])
                    ) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
