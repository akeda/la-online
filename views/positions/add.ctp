<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Position');?>
	<fieldset>
 		<legend><?php __('Tambah Jabatan');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Name', true);?></td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <?php if (!empty($positions)): ?>
            <tr>
                <td class="label-required"><?php echo __('Diverifikasi oleh', true);?></td>
                <td><?php echo $form->select('parent_id', $positions, null, array('escape' => false));?></td>
            </tr>
            <?php endif;?>
            <tr>
                <td class="label-required"><?php echo __('Pemverifikasi Ka Prodi', true);?></td>
                <td>
                <?php
                    echo $form->input('special', array(
                        'type' => 'checkbox',
                        'div'=>false, 'label'=>false, 'class'=>'required'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Add', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
