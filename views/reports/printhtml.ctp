<?php 
    if ($m == 'rekapitulasi') {
        echo "<h1>Rekapitulasi Pengguna Nomor Surat Keluar</h1>";
        echo "<h2>dari input bon nomor surat</h2>";
        $head = "<tr>
        <th>No</th>
        <th>No. Surat</th>
        <th>Kode Surat</th>
        <th>Tgl Surat</th>
        <th>Kepada</th>
        <th>Isi surat/hal</th>
        <th>Ditandatangani oleh</th>
        <th>Unit kerja</th>
        <th>Ket.</th>
        </tr>";
    } else if ($m == 'letter_ins') {
        echo "<h1>Laporan Surat Masuk</h1>";
        $head = "<tr>
        <th>No</th>
        <th>Asal Surat</th>
        <th>No. Surat</th>
        <th>Tgl Surat</th>
        <th>Alamat Pengirim</th>
        <th>Isi surat/hal</th>
        <th>Disposisi kpd</th>
        <th>Unit kerja</th>
        <th>Ket.</th>
        </tr>";
    }
    echo '<br />';
    if ( $per == 'u' ) {
        echo '<span>Unit Kerja: ' . $letters[0]['UnitCode']['name'] . '</span>';
    } else if ( $per == 'p' ) {
        echo '<span>Nama: ' . $letters[0]['User']['name'] . '</span>';
    }
    echo '<span>Per tanggal: ' . date('d/m/Y') . '</span>';
?>
<table>
<thead>
    <?php echo $head;?>
</thead>
<?php
foreach ($letters as $key => $letter) {
    if ($m == 'rekapitulasi') {
        $col2 = $letter['LetterOut']['counter'];
        $col3 = $letter['LetterOut']['letter_no'];
        $col4 = $letter['LetterOut']['letter_date'];
        $col5 = $letter['LetterOut']['letter_to'];
        $col6 = $letter['TermCode']['name'];
        $col7 = $letter['UnitCode']['name'];
        $col8 = $letter['UnitCode']['code'];
        $col9 = $letter['LetterOut']['remark'];
    } else {
        $col2 = $letter['LetterIn']['letter_from'];
        $col3 = $letter['LetterIn']['letter_no'];
        $col4 = $letter['LetterIn']['letter_date'];
        $col5 = $letter['LetterIn']['letter_from_address'];
        $col6 = $letter['LetterIn']['letter_term'];
        $col7 = $letter['LetterIn']['letter_to'];
        $col8 = $letter['UnitCode']['name'];
        $col9 = $letter['LetterIn']['remark'];
    }
?>
    <tr>
        <td><?php echo $key+1;?></td>
        <td><?php echo $col2;?></td>
        <td><?php echo $col3;?></td>
        <td><?php echo $col4;?></td>
        <td><?php echo $col5;?></td>
        <td><?php echo $col6;?></td>
        <td><?php echo $col7;?></td>
        <td><?php echo $col8;?></td>
        <td><?php echo $col9;?></td>
    </tr>
<?php
}
?>
</table>