<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Verificator');?>
	<fieldset>
 		<legend><?php __('Tambah Verificator');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Verificator', true);?></td>
                <td>
                <?php
                    echo $form->select('verificator', $positions, null, array(
                        'class' => 'inputText',
                        'empty' => 'false'
                    ));
                    echo $form->isFieldError('Verificator.verificator') ? $form->error('Verificator.verificator') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Posisi yang diverifikasi', true);?></td>
                <td>
                    <table id="positions_verified" class="positions_verified">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Posisi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if ( isset($positions_verified) ) : ?>
                        <?php
                            foreach ($positions_verified as $key => $pv):
                                echo '<tr class="row_verificators" id="r'.$key.'">';
                                    echo '<td>';
                                    echo '<input type="checkbox" name="data[Verificator]['.$key.'][id]" ' .
                                         'class="cb_verificators" class="inputText" />';
                                    echo '</td>';
                                    
                                    echo '<td>';
                                    echo $form->select(
                                        'position_id', $positions, $pv['item_id'],
                                        array(
                                            'id' => null,
                                            'class' => 'inputText item_id',
                                            'name' => 'data[Asset]['.$key.'][item_id]',
                                            'empty' => false
                                        )
                                    );
                                    echo '</td>';
                                echo '</tr>';
                            endforeach;
                        ?>
                        <?php else: ?>
                        <tr>
                            <td> </td>
                        </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Add', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
