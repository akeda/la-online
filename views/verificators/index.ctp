<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array(
            "fields" => array(
                "verificator" => array(
                    'title' => __('Name', true),
                    'sortable' => true
                ),
                'positions' => array(
                    'title' => __('Jabatan yang diverifikasi', true),
                    'sortable' => true
                )
            ),
            "editable"  => "verificator"
        ));
?>
</div>
