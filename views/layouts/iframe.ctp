<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
 "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
   <title>
    <?php echo $session->read('Site.site_name'); ?>
		<?php echo ' :: ' . $title_for_layout; ?>
   </title>
   <?php
        echo $html->css('rfg.css');
        echo $html->css('style.css');
        echo $html->css('iframe.css');
        echo $html->script('jquery.js');
        echo $html->script('global.js');
        echo $scripts_for_layout;
    ?>
</head>
<body>
<div id="iframe-container">
    <div id="hdflash" role="banner">
    <?php
        if ($session->check('Message.flash')):
            echo $session->flash();
        endif;
    ?>
    </div>
    <?php echo $content_for_layout;?>
    <?php echo $this->element('sql_dump'); ?>
</div>
<div id="loading"></div>
</body>
</html>
