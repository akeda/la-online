<?php if ($show_form): ?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ActList', array('action' => 'report_student_attendance/' . $tpl));?>
	<fieldset>
 		<legend><?php echo 'Rekapitulasi Ketidakhadiran Mahasiswa';?></legend>
        <table class="input">
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Tanggal');?>:</td>
                <td>
                    <?php
                        echo $form->input('date', array(
                            'label' => false, 'div' => false, 'type' => 'date',
                            'dateFormat' => 'MY'
                        ));
                    ?>
                </td>
            </tr>
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Kelas');?>:</td>
                <td>
                    <?php
                        echo $form->select('class', $student_classes, null, array(
                            'empty' => false
                        ));
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<?php else: ?>
<?php echo $html->css('act_lists_index', 'stylesheet', array('inline' => false, 'media' => 'print, screen'));?>
<h2>
REKAPITULASI KETIDAKHADIRAN MAHASISWA<br />
POLITEKNIK MEDIA KREATIF<br />
</h2>
<center>
<table id="tablegrid_act_lists">
    <thead>
        <tr>
            <th colspan="30" class="noborder">
            <table class="noborder">
                <tr>
                    <td class="label">Bulan/Tahun:</td>
                    <td class="val"><?php echo $date;?></td>
                </tr>
                <tr>
                    <td class="label">Kelas:</td>
                    <td class="val"><?php echo $class;?></td>
                </tr>
                <tr>
                    <td class="label">Semester:</td>
                    <td class="val"><?php echo $semester;?></td>
                </tr>
                <tr>
                    <td class="label">Angkatan:</td>
                    <td class="val"><?php echo $year_started;?></td>
                </tr>
            </table>
            </th>
        </tr>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">NIM</th>
            <th rowspan="2">Nama Mahasiswa</th>
            <?php for ($i = 0; $i < 5; $i++):?>
            <th colspan="5" class="center">Minggu <?php echo $i+1;?></th>
            <?php endfor;?>
            <th rowspan="2">Jumlah tidak hadir</th>
            <th rowspan="2">Tindakan (SP)</th>
        </tr>
        <tr>
            <?php for ($i = 0; $i < 25; $i++):?>
                <td><?php echo $week_name[$i%5];?></td>
            <?php endfor;?>
        </tr>
    </thead>
    <tbody>
    <?php if (!empty($students)): ?>
        <?php foreach ($students as $key => $record): ?>
            <tr>
                <td><?php echo $key+1;?></td>
                <td><?php echo $record['Student']['npm'];?></td>
                <td><?php echo $record['Student']['name'];?></td>
                <?php for ($i = 0; $i < 5; $i++):?>
                    <?php for ($j = 1; $j < 6; $j++):?>
                        <td>
                        <?php
                            echo $record['Student']['attendance'][$i][$j] ?
                                 $record['Student']['attendance'][$i][$j] : '';
                        ?>
                        </td>
                    <?php endfor;?>
                <?php endfor;?>
                <td class="right">
                <?php
                    echo $total_absent[$record['Student']['id']] ?
                         $total_absent[$record['Student']['id']] : '';
                ?>
                </td>
                <td class="center"><?php echo $record['Student']['sp'];?></td>
            </tr>
        <?php endforeach;?>
    <?php endif;?>
    </tbody>
    <thead>
        <tr>
            <th colspan="30" class="noborder">
            <table class="noborder">
                <tr>
                    <td class="label"><?php echo $sp[1];?> Jam</td>
                    <td class="val">SP 1</td>
                </tr>
                <tr>
                    <td class="label"><?php echo $sp[2];?> Jam</td>
                    <td class="val">SP 2</td>
                </tr>
                <tr>
                    <td class="label"><?php echo $sp[3];?> Jam</td>
                    <td class="val">SP 3</td>
                </tr>
                <tr>
                    <td class="label"><?php echo $sp[4];?> Jam</td>
                    <td class="val">Diberihentikan studinya/dikeluarkan dari Politeknik</td>
                </tr>
            </table>
            </th>
        </tr>
    </thead>
</table>
</center>
<?php endif;?>
