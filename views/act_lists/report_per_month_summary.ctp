<?php if ($show_form): ?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ActList', array('action' => 'report_per_month_summary'));?>
	<fieldset>
 		<legend><?php echo 'Rekapitulasi Aktivitas Bulanan Per Unit Kerja';?></legend>
        <table class="input">
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Bulan / Tahun');?>:</td>
                <td>
                    <?php
                        echo $form->month('month', date('m'), array(), false);
                        echo '-';
                        echo $form->year('year', 2009, date('Y'), date('Y'));
                    ?>
                </td>
            </tr>
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Unit Kerja');?>:</td>
                <td>
                    <?php
                        echo $form->select('unit_code', $unit_codes, null, array(
                            'empty' => 'All'
                        ));
                    ?>
                    <br />
                    <span class="label">Pilih All untuk menampilkan semua unit kerja</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<?php else: ?>
<?php echo $html->css('act_lists_index', 'stylesheet', array('inline' => false, 'media' => 'print, screen'));?>
<h1>Rekapitulasi Aktifitas Staff Per Unit Kerja</h1>
<center>
<div class="left">
    <table class="noborder">
        <tr>
            <td class="label">Bulan:</td>
            <td class="val"><?php echo $date;?></td>
        </tr>
        <?php if ( isset($unit_code) ): ?>
        <tr>
            <td class="label">Unit Kerja:</td>
            <td class="val"><?php echo $unit_code;?></td>
        </tr>
        <?php endif;?>
    </table>
</div>
<table id="tablegrid_act_lists">
    <thead>
        <?php
            $colspan_head = '';
            $rowspan_head = '';
            if ( count($absent_options) > 0 ):
                $colspan_head = ' colspan="' . count($absent_options) . '"';
                $rowspan_head = ' rowspan="2"';
            endif;
        ?>
        <tr>
            <th<?php echo $rowspan_head;?>>No.</th>
            <th<?php echo $rowspan_head;?>>Nama Staff</th>
            <th<?php echo $rowspan_head;?>>Jabatan</th>
            <th<?php echo $rowspan_head;?>>NIP</th>
            <th<?php echo $rowspan_head;?>>Unit Kerja</th>
            <th<?php echo $rowspan_head;?>>% Kinerja</th>
            <th<?php echo $colspan_head;?> class="center">Absensi</th>
            <th<?php echo $rowspan_head;?>>Insentif yang diterima</th>
            <th<?php echo $rowspan_head;?>>Diverifikasi oleh</th>
        </tr>
        <?php if (!empty($colspan_head)): ?>
        <tr>
        <?php foreach ($absent_options as $absent_nice_name): ?>
            <th><?php echo $absent_nice_name;?></th>
        <?php endforeach;?>
        </tr>
        <?php endif;?>
    </thead>
    <tbody>
    <?php if (!empty($records)): ?>
    <?php
        // init var for number
        $record_no = 0;
    ?>
    <?php foreach ($records as $key => $record): ?>
        <tr>
            <td><?php echo $record_no+1;?></td>
            <td><?php echo $record['CreatedBy']['name'];?></td>
            <td><?php echo $record['CreatedBy']['position_name'];?></td>
            <td><?php echo $record['CreatedBy']['nip'];?></td>
            <td><?php echo $record['CreatedBy']['unit_code_name'];?></td>
            <td><?php echo number_format($record['ActList']['performance'], 2, '.', ',') . '%';?></td>
            <?php foreach ($absent_options as $absent_key => $absent_nice_name): ?>
                <td><?php echo $record['ActList']['absent'][$absent_key];?></td>
            <?php endforeach;?>
            <td><?php echo 'Rp ' . number_format($record['ActList']['insentive'], 2, '.', ',');?></td>
            <td><?php echo $record['ActList']['verificator'];?></td>
        </tr>
    <?php $record_no++;?>
    <?php endforeach;?>
    <?php endif;?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5" class="right">TOTAL</td>
            <td><?php echo number_format($all_total_performance, 2, '.', ',');?> %</td>
            
            <?php foreach ($absent_options as $absent_key => $absent_nice_name): ?>
                <td><?php echo $all_total_absent[$absent_key];?></td>
            <?php endforeach;?>
            
            <td>Rp <?php echo number_format($all_total_insentive, 2, '.', ',');?></td>
            <td>&nbsp;</td>
        </tr>
    </tfoot>
</table>
</center>
<?php endif;?>
