<?php echo $html->script('act_lists.js?201102091', array('inline' => false));?>
<?php echo $javascript->codeBlock('var getStudents = "' . $getStudents . '";', array('inline' => false));?>
<?php echo $javascript->codeBlock('var getCourses = "' . $getCourses . '";', array('inline' => false));?>
<?php echo $javascript->codeBlock('var special_activities = ' . json_encode($special_activities) . ';', array('inline' => false));?>
<?php echo $html->css('act_lists.css?20110131', 'stylesheet', array('inline' => false));?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('ActList');?>
	<fieldset>
 		<legend><?php __('Input Kegiatan Hari ini');?></legend>
        <table class="input">
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="label">Nama</td>
                            <td class="val"><?php echo $profile['name'];?></td>
                        </tr>
                        <tr>
                            <td class="label">NIP</td>
                            <td class="val"><?php echo $profile['nip'];?></td>
                        </tr>
                        <tr>
                            <td class="label">Jabatan</td>
                            <td class="val"><?php echo $my_position;?></td>
                        </tr>
                        <tr>
                            <td class="label">Aktifitas pada tanggal</td>
                            <!-- manual date for temp as requested by Mr. Heru -->
                            <td class="val">
                            <?php
                                //echo date('d/m/Y');
                                echo $form->input('created', array(
                                    'type' => 'date', 'div' => false,
                                    'label' => false,
                                ));
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">Akan diverfikasi oleh</td>
                            <td class="val"><?php echo $verificator_position;?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table id="list_activities" class="list_activities">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Kegiatan</th>
                                <th>Kelas</th>
                                <th>Mata Kuliah</th>
                                <th>Waktu Kuliah</th>
                                <th>Mahasiswa yang tidak hadir</th>
                                <th colspan="2" class="center">Uraian Materi</th>
                                <th>No SK</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if ( isset($lists) ): ?>
                        <?php
                            foreach ($lists['ActListActivity'] as $key => $list_activity):
                                echo '<tr class="row_list_activities" id="r'.$key.'" rel="r'.$key.'_d0">';
                                    echo '<td>' .
                                         '<input type="checkbox" name="data[ActList][ActListActivity]['.$key.'][id]" class="cb_list_activities" class="inputText" />' .
                                        '</td>';
                                    echo '<td>';
                                    echo $form->select('activity_id', $activities, $list_activity['activity_id'], array(
                                            'id' => null, 'class' => 'activity_id',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][activity_id]',
                                            'empty' => false
                                        ));
                                    echo '</td>';
                                    
                                    echo '<td class="specialCol">';
                                    echo $form->select('student_class_id', $student_classes, $list_activity['ActListActivityDescription'][0]['student_class_id'], array(
                                            'id' => null, 'class' => 'student_class_id',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][student_class_id]',
                                            'empty' => true
                                        ));
                                    echo '</td>';
                                    
                                    echo '<td class="specialCol">';
                                    echo $form->select('course_id', $courses, $list_activity['ActListActivityDescription'][0]['course_id'], array(
                                            'id' => null, 'class' => 'course_id',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][course_id]',
                                            'empty' => true
                                        ));
                                    echo '</td>';
                                    
                                    echo '<td class="specialCol"><span class="nowrap">';
                                    echo $form->select('start_class', $class_hours, $list_activity['ActListActivityDescription'][0]['start_class'], array(
                                        'id' => null, 'class' => 'start_class',
                                        'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][start_class]',
                                        'empty' => true
                                    )) . ' s/d ';
                                    echo $form->select('end_class', $class_hours, $list_activity['ActListActivityDescription'][0]['end_class'], array(
                                        'id' => null, 'class' => 'end_class',
                                        'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][end_class]',
                                        'empty' => true
                                    ));
                                    echo '</span></td>';
                                    echo '<td class="student_lists specialCol"></td>';
                                    echo '<td>';
                                    echo $html->image('cross.png', array(
                                            'alt' => 'Klik untuk menghapus sub kegiatan',
                                            'rel' => 'r'.$key.'_d0',
                                            'class' => 'del_description'
                                        ));
                                    echo '</td>';
                                    echo '<td class="specialCol">';
                                    echo $form->input('description',
                                            array(
                                                'id' => null,
                                                'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][description]',
                                                'class' => 'inputText description',
                                                'type' => 'textarea', 'div' => false, 'label' => false,
                                                'value' => $list_activity['ActListActivityDescription'][0]['description']
                                            )
                                        );
                                    echo '</td>';
                                    echo '<td class="specialCol">';
                                    echo $form->input('assignment_no', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText assignment_no',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][assignment_no]',
                                            'value' => $list_activity['ActListActivityDescription'][0]['assignment_no']
                                        ));
                                    echo '</td>';
                                echo '</tr>';
                                
                                // cek list_activity_description and make sure > 1
                                if (!empty($list_activity['ActListActivityDescription']) && count($list_activity['ActListActivityDescription']) > 1) {
                                    // iterate each description
                                    foreach ($list_activity['ActListActivityDescription'] as $key2 => $list_activity_description):
                                        if ($key2 === 0) {
                                            continue;
                                        }
                                        
                                        echo '<tr class="row_list_activity_descriptions" id="r'.$key.'_d'.$key2.'" rel="r'.$key.'_d'.$key2.'">';
                                        echo '<td class="empty">&nbsp;</td>';
                                        echo '<td class="empty">&nbsp;</td>';
                                        echo '<td class="specialCol">';
                                        echo $form->select('student_class_id', $student_classes, $list_activity_description['student_class_id'], array(
                                                'id' => null, 'class' => 'student_class_id',
                                                'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][student_class_id]',
                                                'empty' => true
                                            ));
                                        echo '</td>';
                                        echo '<td class="specialCol">';
                                        echo $form->select('course_id', $courses, $list_activity_description['course_id'], array(
                                                'id' => null, 'class' => 'course_id',
                                                'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][course_id]',
                                                'empty' => true
                                            ));
                                        echo '</td>';
                                        echo '<td class="specialCol"><span class="nowrap">';
                                        echo $form->select('start_class', $class_hours, $list_activity_description['start_class'], array(
                                            'id' => null, 'class' => 'start_class',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][start_class]',
                                            'empty' => true
                                        )) . ' s/d ';
                                        echo $form->select('end_class', $class_hours, $list_activity_description['end_class'], array(
                                            'id' => null, 'class' => 'end_class',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][end_class]',
                                            'empty' => true
                                        ));
                                        echo '</span></td>';
                                        echo '<td class="student_lists specialCol"></td>';
                                        echo '<td>';
                                        echo $html->image('cross.png', array(
                                                'alt' => 'Klik untuk menghapus sub kegiatan',
                                                'rel' => 'r'.$key.'_d'.$key2,
                                                'class' => 'del_description'
                                            ));
                                        echo '</td>';
                                        echo '<td class="specialCol">';
                                        echo $form->input('description',
                                                array(
                                                    'id' => null,
                                                    'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][description]',
                                                    'class' => 'inputText description',
                                                    'type' => 'textarea', 'div' => false, 'label' => false,
                                                    'value' => $list_activity_description['description']
                                                )
                                            );
                                        echo '</td>';
                                        echo '<td class="specialCol">';
                                        echo $form->input('assignment_no', array(
                                                'div' => false, 'label' => false,
                                                'id' => null, 'class' => 'inputText assignment_no',
                                                'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][assignment_no]',
                                                'value' => $list_activity_description['assignment_no']
                                            ));
                                        echo '</td>';
                                        echo '</tr>';
                                    endforeach;
                                } // eof check list_activity_description
                                
                                // add .add_description row (latest row)
                                echo '<tr id="rg'.$key.'" class="add_description_row">' .
                                        '<td colspan="2">&nbsp;</td>
                                        <td colspan="5">
                                            <span class="up"></span>
                                            <input type="button" class="add_description" id="b'.$key.'" value="+ Tambah sub kegiatan" />
                                        </td>' .
                                     '</tr>';
                            endforeach; // eof list_activity
                        ?>
                        <?php else: ?>
                            <tr class="row_list_activities" id="r0" rel="r0_d0">
                                <td>
                                    <input type="checkbox" name="data[ActList][ActListActivity][0][id]" class="cb_list_activities" class="inputText" />
                                </td>                         
                                <td> 
                                    <?php 
                                        echo $form->select(
                                        'activity_id', $activities, null,
                                        array(
                                            'id' => null,
                                            'class' => 'activity_id',
                                            'name' => 'data[ActList][ActListActivity][0][activity_id]',
                                            'empty' => false
                                        ));
                                    ?>
                                </td>
                                <td class="specialCol">
                                    <?php 
                                        echo $form->select(
                                        'student_class_id', $student_classes, null,
                                        array(
                                            'id' => null,
                                            'class' => 'student_class_id',
                                            'name' => 'data[ActList][ActListActivity][0][ActListActivityDescription][0][student_class_id]',
                                            'empty' => true
                                        ));
                                    ?>
                                </td>
                                <td class="specialCol">
                                    <?php 
                                        echo $form->select(
                                        'course_id', $courses, null,
                                        array(
                                            'id' => null,
                                            'class' => 'course_id',
                                            'name' => 'data[ActList][ActListActivity][0][ActListActivityDescription][0][course_id]',
                                            'empty' => true
                                        ));
                                    ?>
                                </td>
                                <td class="specialCol">
                                    <span class="nowrap">
                                    <?php
                                        echo $form->select(
                                        'start_class', $class_hours, null,
                                        array(
                                            'id' => null,
                                            'class' => 'start_class',
                                            'name' => 'data[ActList][ActListActivity][0][ActListActivityDescription][0][start_class]',
                                            'empty' => true
                                        )) . ' s/d ';
                                        echo $form->select(
                                        'end_class', $class_hours, null,
                                        array(
                                            'id' => null,
                                            'class' => 'end_class',
                                            'name' => 'data[ActList][ActListActivity][0][ActListActivityDescription][0][end_class]',
                                            'empty' => true
                                        ))
                                    ?>
                                    </span>
                                </td>
                                <td class="student_lists specialCol"></td>
                                <td>
                                    <?php
                                        echo $html->image('cross.png', array(
                                            'alt' => 'Klik untuk menghapus sub kegiatan',
                                            'rel' => 'r0_d0',
                                            'class' => 'del_description'
                                        ));
                                    ?>
                                </td>
                                <td class="specialCol">
                                    <?php
                                        echo $form->input('description',
                                            array(
                                                'id' => null,
                                                'name' => 'data[ActList][ActListActivity][0][ActListActivityDescription][0][description]',
                                                'class' => 'inputText description',
                                                'type' => 'textarea', 'div' => false, 'label' => false
                                            )
                                        );
                                    ?>
                                </td>
                                <td class="specialCol">
                                    <?php
                                        echo $form->input('assignment_no', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText assignment_no',
                                            'name' => 'data[ActList][ActListActivity][0][ActListActivityDescription][0][assignment_no]'
                                        ));
                                    ?>
                                </td>
                            </tr>
                            <tr id="rg0" class="add_description_row">
                                <td colspan="2">&nbsp;</td>
                                <td colspan="7">
                                    <span class="up"></span>
                                    <input type="button" class="add_description" id="b0" value="+ Tambah sub kegiatan" />
                                </td>
                            </tr>
                        <?php endif;?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>&nbsp;</th>
                                <th colspan="9">
                                    <span class="up"></span>
                                    <input type="button" name="add_row" id="add_row" value="+ Tambah Kegiatan" /> &nbsp; atau &nbsp;
                                    <input type="button" name="del_row" id="del_row" value="- Hapus Kegiatan" />
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Simpan Aktifitas ini', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>

