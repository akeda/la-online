<?php echo $html->css('act_lists_index', 'stylesheet', array('inline' => false));?>
<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "created_by" => array(
                    'title' => __("Nama", true), 
                    'sortable' => true
                ),
                "position_name" => array(
                    'title' => __("Jabatan", true), 
                    'sortable' => true
                ),
                "unit_code_name" => array(
                    'title' => __("Unit Kerja", true),
                    'sortable' => true
                ),
                "activity_date" => array(
                    'title' => __("Tgl. Aktifitas", true),
                    'sortable' => true
                ),
                "activity" => array(
                    'title' => __("Aktifitas", true),
                    'sortable' => false,
                    'wrapBefore' => '<div class="activities">',
                    'wrapAfter' => '</div>'
                ),
                "created" => array(
                    'title' => __("Created On", true),
                    'sortable' => false
                ),
                'verified' => array(
                    'title' => __("Waktu Verifikasi Prodi", true),
                    'sortable' => false
                ),
                'approved' => array(
                    'title' => __("Waktu Verifikasi BAAK", true),
                    'sortable' => false
                )
              ),
              "editable"  => "created_by",
              "editableArgs"  => array(
                'verify' => 1
              ),
              "assoc" => array(
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'CreatedBy'
                ),
                'position_name' => array(
                    'field' => 'position_name',
                    'model' => 'CreatedBy'
                ),
                'unit_code_name' => array(
                    'field' => 'unit_code_name',
                    'model' => 'CreatedBy'
                )
              ),
              'if' => array(
                'verified' => array(
                    'operator' => 'fieldsEqualTo',
                    'fields' => array(
                        array(
                            'name' => 'verified',
                            'expected' => '',
                            'showAs' => '<span class="strong red"><strong>Belum di verifikasi</strong></span>'
                        )
                    )
                ),
                'approved' => array(
                    'operator' => 'fieldsEqualTo',
                    'fields' => array(
                        array(
                            'name' => 'approved',
                            'expected' => '',
                            'showAs' => '<span class="strong red"><strong>Belum di verifikasi</strong></span>'
                        )
                    )
                )
              ),
              'displayedAs' => array(
                'activity_date' => 'date',
                'created' => 'datetime'
              )
        ));
?>
</div>
