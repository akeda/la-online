<?php if ($show_form): ?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ActList', array('action' => 'report_honor_per_month'));?>
	<fieldset>
 		<legend><?php echo 'Rekapitulasi Honorarium Bulanan Staff Pengajar';?></legend>
        <table class="input">
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Bulan / Tahun');?>:</td>
                <td>
                    <?php
                        echo $form->month('month', date('m'), array(), false);
                        echo '-';
                        echo $form->year('year', 2009, date('Y'), date('Y'));
                    ?>
                </td>
            </tr>
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Prodi');?>:</td>
                <td>
                    <?php
                        echo $form->select('unit_code', $unit_codes, null, array(
                            'empty' => 'All'
                        ));
                    ?>
                    <br />
                    <span class="label">Pilih All untuk menampilkan semua prodi</span>
                </td>
            </tr>
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Nama Staff');?>:</td>
                <td>
                    <?php
                        echo $form->select('user', $users, null, array(
                            'empty' => 'All'
                        ));
                    ?>
                    <br />
                    <span class="label">Pilih All untuk menampilkan semua staff</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<?php else: ?>
<?php echo $html->css('act_lists_index', 'stylesheet', array('inline' => false, 'media' => 'print, screen'));?>
<center>
<table id="tablegrid_act_lists">
    <thead>
        <tr>
            <th colspan="11" class="honor_header">
DAFTAR PEMBAYARAN HONORARIUM STAF PENGAJAR<br />
<?php echo $uc ? 'PRODI ' . $uc . '<br />' : ''; ?>
DALAM RANGKA PENYELENGGARAAN PROGRAM PENDIDIKAN TINGGI<br />
POLITEKNIK NEGERI MEDIA KREATIF JAKARTA<br /><br />
BULAN <?php echo $month_name . ' ' . $year_name; ?>
            </th>
        </tr>
    </thead>
    <thead>
        <tr>
            <th>No.</th>
            <th>NAMA</th>
            <th>Jml Jam</th>
            <th>Honor Per Jam</th>
            <th>JML HADIR</th>
            <th>HADIR PER KEHADIRAN Rp</th>
            <th>JUMLAH TOTAL Rp</th>
            <th>JUMLAH HONOR Rp</th>
            <th>PPh. 21 5 % Rp</th>
            <th>Jumlah Diterima</th>
            <th>TANDA TANGAN</th>
        </tr>
    </thead>
    <tbody>
    <?php if (!empty($records)): ?>
    <?php $no = 0; ?>
    <?php foreach ($records as $key => $record): ?>
        <tr>
            <td class="no"><?php echo ++$no;?></td>
            <td class="numeric"><?php echo $record['name'];?></td>
            <td class="numeric"><?php echo $record['total_hours'];?></td>
            <td class="numeric"><?php echo $record['honor_teaching'] ? '50,000' : '';?></td>
            <td class="numeric"><?php echo $record['honor_attendance'] ? $record['total_attendance'] : '';?></td>
            <td class="numeric"><?php echo $record['honor_attendance'] ? '50,000' : '';?></td>
            <td class="numeric">
            <?php
                echo number_format($record['honor_per_hour'], 0, '', ',') . '<br />';
                echo $record['honor_attendance'] ? number_format($record['honor_per_attendance'], 0, '', ',') : '';
            ?>
            </td>
            <td class="numeric">
            <?php
                echo $record['total_honor'] ? number_format($record['total_honor'], 0, '', ',') : '';
            ?>
            </td>
            <td class="numeric">
            <?php
                echo $record['pph21'] ? number_format($record['pph21'], 0, '', ',') : '';
            ?>
            </td>
            <td class="numeric">
            <?php
                echo $record['total_received'] ? number_format($record['total_received'], 0, '', ',') : '';
            ?>
            </td>
            <td></td>
        </tr>
    <?php endforeach;?>
    <?php endif;?>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="7" class="no">JUMLAH</th>
            <th class="numeric">
            <?php
                echo $totals['honor'] ? number_format($totals['honor'], 0, '', ',') : '';
            ?>
            </th>
            <th class="numeric">
            <?php
                echo $totals['pph21'] ? number_format($totals['pph21'], 0, '', ',') : '';
            ?>
            </th>
            <th class="numeric">
            <?php
                echo $totals['received'] ? number_format($totals['received'], 0, '', ',') : '';
            ?>
            </th>
            <th></th>
        </tr>
    </tfoot>
</table>
</center>
<?php endif;?>
