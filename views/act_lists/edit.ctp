<?php echo $html->script('act_lists.js?201102091', array('inline' => false));?>
<?php echo $javascript->codeBlock('var getStudents = "' . $getStudents . '";', array('inline' => false));?>
<?php echo $javascript->codeBlock('var getCourses = "' . $getCourses . '";', array('inline' => false));?>
<?php echo $javascript->codeBlock('var special_activities = ' . json_encode($special_activities) . ';', array('inline' => false));?>
<?php echo $html->css('act_lists.css?20110131', 'stylesheet', array('inline' => false));?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('ActList', array('action' => 'edit'));?>
	<fieldset>
 		<legend><?php __('Edit Kegiatan Harian');?></legend>
        <table class="input">
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="label">Nama</td>
                            <td class="val"><?php echo $creator_name;?></td>
                        </tr>
                        <tr>
                            <td class="label">Jabatan</td>
                            <td class="val"><?php echo $my_position;?></td>
                        </tr>
                        <tr>
                            <td class="label">Aktifitas pada tanggal</td>
                            <td class="val">
                            <?php
                                echo $form->input('created', array(
                                    'type' => 'date', 'div' => false,
                                    'label' => false,
                                ));
                                // echo $time->format('d/m/Y', $activity_date);
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">Verifikator Prodi</td>
                            <td class="val"><?php echo $verificator_position;?></td>
                        </tr>
                        <tr>
                            <td class="label">Verifikator BAAK</td>
                            <td class="val"><?php echo $approver_position['name'];?></td>
                        </tr>
                        <?php if (!$not_user):?>
                        <tr>
                            <td class="label">Verifikasi Kehadiran</td>
                            <td class="val">
                            <?php
                                echo $absent_options[$absent];
                            ?>
                            </td>
                        </tr>
                        <?php endif;?>
                    </table>
                </td>
            </tr>
            <?php if ($not_user):?>
            <tr>
                <td colspan="2">
                    <div class="flash_box flash_info">
                    Saya sebagai verifikator menyetujui untuk memverifikasi kegiatan bawahan saya
                    dengan sebenar-benarnya.<br />Sebagai verifikator hal yang perlu dilakukan : <br />
                    1. Set kehadiran pada tanggal aktifitas.<br />
                    2. Jika kegiatan benar, lakukan centang pada kolom Verifikasi kebenaran.
                    </ol>
                    </div>
                    Absensi <?php echo $creator_name;?> pada tanggal <?php echo $time->format('d/m/Y', $activity_date);?> : &nbsp;
                    <?php
                    echo $form->select('absent', $absent_options, null,
                        array('div' => false, 'label' => false, 'empty' => false)
                    );
                    ?>
                </td>
            </tr>
            <?php endif;?>
            <tr>
                <td colspan="2">
                    <table id="list_activities" class="list_activities">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Kegiatan</th>
                                <th>Kelas</th>
                                <th>Mata Kuliah</th>
                                <th>Waktu Kuliah</th>
                                <th>Mahasiswa yang tidak hadir</th>
                                <th colspan="2" class="center">Uraian Materi</th>
                                <th>No SK</th>
                                <th>Verifikasi Prodi</th>
                                <th>Verifikasi BAAK</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if ( isset($lists) ): ?>
                        <?php
                            $enable_to_verify_all = true;
                            $enable_to_verify = true;
                            foreach ($lists['ActListActivity'] as $key => $list_activity):
                                echo '<tr class="row_list_activities" id="r'.$key.'" rel="r'.$key.'_d0">';
                                    echo '<td>';
                                    if ($not_user):
                                        // hide it
                                        echo '<input type="hidden" name="data[ActList][ActListActivity]['. $key .
                                             '][id]" class="cb_list_activities" class="inputText" />';
                                    else:
                                        echo '<input type="checkbox" name="data[ActList][ActListActivity]['. $key .
                                             '][id]" class="cb_list_activities" class="inputText" />';
                                    endif;
                                    echo '</td>';
                                    echo '<td>';
                                    if ($not_user):
                                        echo $activities[ $list_activity['activity_id'] ];
                                        // hide it
                                        echo $form->input('activity_id', array(
                                            'id' => null, 'class' => 'activity_id',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][activity_id]',
                                            'type' => 'hidden', 'value' => $list_activity['activity_id']
                                        ));
                                    else:
                                        echo $form->select('activity_id', $activities, $list_activity['activity_id'], array(
                                            'id' => null, 'class' => 'activity_id',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][activity_id]',
                                            'empty' => false
                                        ));
                                    endif;
                                    echo '</td>';
                                    
                                    echo '<td class="specialCol">';
                                    if ($not_user):
                                        // hide it
                                        if ( isset($student_classes[$list_activity['ActListActivityDescription'][0]['student_class_id']]) ):
                                            echo $student_classes[$list_activity['ActListActivityDescription'][0]['student_class_id']];
                                        endif;
                                        echo $form->input('student_class_id', array(
                                            'id' => null, 'class' => 'student_class_id',
                                            'type' => 'hidden', 'value' => $list_activity['ActListActivityDescription'][0]['student_class_id'],
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][student_class_id]'
                                        ));
                                    else:
                                        echo $form->select('student_class_id', $student_classes, $list_activity['ActListActivityDescription'][0]['student_class_id'], array(
                                            'id' => null, 'class' => 'student_class_id',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][student_class_id]',
                                            'empty' => true
                                        ));
                                    endif;
                                    echo '</td>';
                                    
                                    echo '<td class="specialCol">';
                                    if ($not_user):
                                        // hide it
                                        if ( isset($courses[$list_activity['ActListActivityDescription'][0]['course_id']]) ):
                                            echo $courses[$list_activity['ActListActivityDescription'][0]['course_id']];
                                        endif;
                                        echo $form->input('course_id', array(
                                            'id' => null, 'class' => 'course_id',
                                            'type' => 'hidden', 'value' => $list_activity['ActListActivityDescription'][0]['course_id'],
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][course_id]'
                                        ));
                                    else:
                                        echo $form->select('course_id', $courses, $list_activity['ActListActivityDescription'][0]['course_id'], array(
                                            'id' => null, 'class' => 'course_id',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][course_id]',
                                            'empty' => true
                                        ));
                                    endif;
                                    echo '</td>';
                                    
                                    echo '<td class="specialCol"><span class="nowrap">';
                                    if ($not_user):
                                        if ( !empty($list_activity['ActListActivityDescription'][0]['start_class']) && !empty($list_activity['ActListActivityDescription'][0]['end_class']) ):
                                            echo $class_hours[$list_activity['ActListActivityDescription'][0]['start_class']] . ' s/d ' .
                                                 $class_hours[$list_activity['ActListActivityDescription'][0]['end_class']];
                                        endif;
                                        // hide it
                                        echo $form->input('start_class', array(
                                            'id' => null, 'class' => 'start_class',
                                            'type' => 'hidden', 'value' => $list_activity['ActListActivityDescription'][0]['start_class'],
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][start_class]'
                                        ));
                                        echo $form->input('end_class', array(
                                            'id' => null, 'class' => 'end_class',
                                            'type' => 'hidden', 'value' => $list_activity['ActListActivityDescription'][0]['end_class'],
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][end_class]'
                                        ));
                                    else:
                                        echo $form->select('start_class', $class_hours, $list_activity['ActListActivityDescription'][0]['start_class'], array(
                                            'id' => null, 'class' => 'start_class',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][start_class]',
                                            'empty' => true
                                        )) . ' s/d ';
                                        echo $form->select('end_class', $class_hours, $list_activity['ActListActivityDescription'][0]['end_class'], array(
                                            'id' => null, 'class' => 'end_class',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][end_class]',
                                            'empty' => true
                                        ));
                                    endif;
                                    echo '</span></td>';
                                    
                                    echo '<td class="student_lists specialCol">';
                                    $sids = array();
                                    foreach ($list_activity['ActListActivityDescription'][0]['ActListActivityDescriptionStudent'] as $sid) {
                                        $sids[] = $sid['student_id'];
                                    }
                                    if ($not_user):
                                        if ( isset($list_activity['ActListActivityDescription'][0]['StudentClass']['Student']) && !empty($list_activity['ActListActivityDescription'][0]['StudentClass']['Student']) ):
                                            foreach ($list_activity['ActListActivityDescription'][0]['StudentClass']['Student'] as $skey => $sid) {
                                                // hide it
                                                if ( in_array($sid['id'], $sids) ) {
                                                    echo '<span class="nowrap">' . $sid['name'] . ' (' . $sid['npm'] . ')</span><br />';
                                                    echo '<input type="hidden" value="'. $sid['id'] .'" name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][student_id]['.$skey.']" />';
                                                }
                                            }
                                        endif;
                                    else:
                                        if ( isset($list_activity['ActListActivityDescription'][0]['StudentClass']['Student']) && !empty($list_activity['ActListActivityDescription'][0]['StudentClass']['Student']) ):
                                            echo '<table>';
                                                foreach ($list_activity['ActListActivityDescription'][0]['StudentClass']['Student'] as $ksid => $sid) {
                                                    echo '<tr><td>';
                                                    if ( in_array($sid['id'], $sids) ) {
                                                        echo '<input type="checkbox" name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][student_id]['.$ksid.']" checked="checked" value="'. $sid['id'] .'" />';
                                                    } else {
                                                        echo '<input type="checkbox" name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][student_id]['.$ksid.']" value="'. $sid['id'] .'" />';
                                                    }
                                                    echo '</td>';
                                                    echo '<td class="nowrap">'.$sid['name'] . ' (' . $sid['npm'] . ')</td></tr>';
                                                }
                                            echo '</table>';
                                            echo '<span class="label">Centang yang tidak hadir</span>';
                                        endif;
                                    endif;
                                    echo '</td>';
                                    
                                    echo '<td>';
                                    if ($not_user):
                                        echo '&nbsp;';
                                    else:
                                        echo $html->image('cross.png', array(
                                            'alt' => 'Klik untuk menghapus sub kegiatan',
                                            'rel' => 'r'.$key.'_d0',
                                            'class' => 'del_description'
                                        ));
                                    endif;
                                    echo '</td>';
                                    echo '<td class="specialCol">';
                                    if ($not_user):
                                        echo $list_activity['ActListActivityDescription'][0]['description'];
                                        // hide it
                                        echo $form->input('description',
                                            array(
                                                'id' => null,
                                                'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][description]',
                                                'class' => 'inputText description',
                                                'type' => 'hidden', 'div' => false, 'label' => false,
                                                'value' => $list_activity['ActListActivityDescription'][0]['description']
                                            )
                                        );
                                    else:
                                        echo $form->input('description',
                                            array(
                                                'id' => null,
                                                'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][description]',
                                                'class' => 'inputText description',
                                                'type' => 'textarea', 'div' => false, 'label' => false,
                                                'value' => $list_activity['ActListActivityDescription'][0]['description']
                                            )
                                        );
                                    endif;
                                    echo '</td>';
                                    
                                    echo '<td class="specialCol">';
                                    if ($not_user):
                                        echo $list_activity['ActListActivityDescription'][0]['assignment_no'];
                                        // hide it
                                        echo $form->input('assignment_no', array(
                                            'id' => null, 'class' => 'assignment_no',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][assignment_no]',
                                            'type' => 'hidden',
                                            'value' => $list_activity['ActListActivityDescription'][0]['assignment_no']
                                        ));
                                    else:
                                        echo $form->input('assignment_no', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText assignment_no',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][assignment_no]',
                                            'value' => $list_activity['ActListActivityDescription'][0]['assignment_no']
                                        ));
                                    endif;
                                    echo '</td>';
                                    
                                    echo '<td class="center">';
                                    if ($not_user && !$approver):
                                        if ( $list_activity['ActListActivityDescription'][0]['StudentClass']['study_program_id'] == $currentUnitCodeId ):
                                            echo '<input type="checkbox" value="1"' .
                                                ' name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][verified]"'.
                                                ' class="inputText verified"' .
                                                ($list_activity['ActListActivityDescription'][0]['verified'] ? ' checked="checked"' : '') .
                                                ' />';
                                        else:
                                            echo $list_activity['ActListActivityDescription'][0]['verified'] ?
                                                '<span class="green">Sudah diverifikasi prodi bersangkutan</span>' :
                                                '<span class="red">Akan diverifikasi prodi bersangkutan</span>';
                                            // hide it
                                            echo $form->input('verified', array(
                                                'id' => null, 'class' => 'verified', 'type' => 'hidden',
                                                'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][verified]',
                                                'value' => $list_activity['ActListActivityDescription'][0]['verified']
                                            ));
                                        endif;
                                    else:
                                        if ($approver):
                                            echo '<input type="checkbox" value="1"' .
                                                ' name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][verified]"'.
                                                ' class="inputText verified"' .
                                                ($list_activity['ActListActivityDescription'][0]['verified'] ? ' checked="checked"' : '') .
                                                ' />';
                                        else:
                                            echo $list_activity['ActListActivityDescription'][0]['verified'] ?
                                                 '<span class="green">Sudah diverifikasi</span>' :
                                                 '<span class="red">Belum diverifikasi</span>';
                                            // hide it
                                            echo $form->input('verified', array(
                                                    'id' => null, 'class' => 'verified', 'type' => 'hidden',
                                                    'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][verified]',
                                                    'value' => $list_activity['ActListActivityDescription'][0]['verified']
                                                ));
                                        endif;
                                    endif;
                                    echo '</td>';
                                    
                                    echo '<td class="center">';
                                    if ($approver):
                                        echo '<input type="checkbox" value="1"' .
                                            ' name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][approved]"'.
                                            ' class="inputText approved"' .
                                            ($list_activity['ActListActivityDescription'][0]['approved'] ? ' checked="checked"' : '') .
                                            ' />';
                                    else:
                                        echo $list_activity['ActListActivityDescription'][0]['approved'] ?
                                             '<span class="green">Sudah diverifikasi</span>' :
                                             '<span class="red">Belum diverifikasi</span>';
                                        // hide it
                                        echo $form->input('approved', array(
                                            'id' => null, 'class' => 'approved', 'type' => 'hidden',
                                            'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription][0][approved]',
                                            'value' => $list_activity['ActListActivityDescription'][0]['approved']
                                        ));
                                    endif;
                                    echo '</td>';
                                    
                                echo '</tr>';
                                
                                // cek list_activity_description and make sure > 1
                                if (!empty($list_activity['ActListActivityDescription']) && count($list_activity['ActListActivityDescription']) > 1) {
                                    // iterate each description
                                    foreach ($list_activity['ActListActivityDescription'] as $key2 => $list_activity_description):
                                        if ($key2 === 0) {
                                            continue;
                                        }
                                        
                                        echo '<tr class="row_list_activity_descriptions" id="r'.$key.'_d'.$key2.'" rel="r'.$key.'_d'.$key2.'">';
                                        echo '<td class="empty">&nbsp;</td>';
                                        echo '<td class="empty">&nbsp;</td>';
                                        
                                        echo '<td class="specialCol">';
                                        if ($not_user):
                                            if ( isset($student_classes[$list_activity_description['student_class_id']]) ):
                                                echo $student_classes[$list_activity_description['student_class_id']];
                                            endif;
                                            // hide it
                                            echo $form->input('student_class_id', array(
                                                    'id' => null, 'class' => 'student_class_id',
                                                    'type' => 'hidden', 'value' => $list_activity_description['student_class_id'],
                                                    'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][student_class_id]'
                                                ));
                                        else:
                                            echo $form->select('student_class_id', $student_classes, $list_activity_description['student_class_id'], array(
                                                    'id' => null, 'class' => 'student_class_id',
                                                    'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][student_class_id]',
                                                    'empty' => true
                                                ));
                                        endif;
                                        echo '</td>';
                                        
                                        echo '<td class="specialCol">';
                                        if ($not_user):
                                            if ( isset($courses[$list_activity_description['course_id']]) ):
                                                echo $courses[$list_activity_description['course_id']];
                                            endif;
                                            // hide it
                                            echo $form->input('course_id', array(
                                                    'id' => null, 'class' => 'course_id',
                                                    'type' => 'hidden', 'value' => $list_activity_description['course_id'],
                                                    'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][course_id]'
                                                ));
                                        else:
                                            echo $form->select('course_id', $courses, $list_activity_description['course_id'], array(
                                                    'id' => null, 'class' => 'course_id',
                                                    'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][course_id]',
                                                    'empty' => true
                                                ));
                                        endif;
                                        echo '</td>';
                                        
                                        echo '<td class="specialCol">';
                                        if ($not_user):
                                            if ( !empty($list_activity_description['start_class']) && !empty($list_activity_description['end_class']) ):
                                                echo $class_hours[$list_activity_description['start_class']] . ' s/d ' .
                                                     $class_hours[$list_activity_description['end_class']];
                                            endif;
                                            // hide it
                                            echo $form->input('start_class', array(
                                                'id' => null, 'class' => 'start_class',
                                                'type' => 'hidden', 'value' => $list_activity_description['start_class'],
                                                'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][start_class]'
                                            ));
                                            echo $form->input('end_class', array(
                                                'id' => null, 'class' => 'end_class',
                                                'type' => 'hidden', 'value' => $list_activity_description['end_class'],
                                                'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][end_class]'
                                            ));
                                        else:
                                            echo $form->select('start_class', $class_hours, $list_activity_description['start_class'], array(
                                                'id' => null, 'class' => 'start_class',
                                                'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][start_class]',
                                                'empty' => true
                                            )) . ' s/d ';
                                            echo $form->select('end_class', $class_hours, $list_activity_description['end_class'], array(
                                                'id' => null, 'class' => 'end_class',
                                                'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][end_class]',
                                                'empty' => true
                                            ));
                                        endif;
                                        echo '</td>';
                                        
                                        echo '<td class="student_lists specialCol">';
                                        $sids = array();
                                        foreach ($list_activity_description['ActListActivityDescriptionStudent'] as $sid) {
                                            $sids[] = $sid['student_id'];
                                        }
                                        if ($not_user):
                                            if ( isset($list_activity_description['StudentClass']['Student']) && !empty($list_activity_description['StudentClass']['Student']) ):
                                                foreach ($list_activity_description['StudentClass']['Student'] as $ksid => $sid) {
                                                    // hide it
                                                    if ( in_array($sid['id'], $sids) ) {
                                                        echo '<span class="nowrap">' . $sid['name'] . ' (' . $sid['npm'] . ')</span><br />';
                                                        echo '<input type="hidden" value="'. $sid['id'] .'" name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][student_id]['.$ksid.']" />';
                                                    }
                                                }
                                            endif;
                                        else:
                                            if ( isset($list_activity_description['StudentClass']['Student']) && !empty($list_activity_description['StudentClass']['Student']) ):
                                                echo '<table>';
                                                foreach ($list_activity_description['StudentClass']['Student'] as $ksid => $sid) {
                                                    echo '<tr><td>';
                                                    if ( in_array($sid['id'], $sids) ) {
                                                        echo '<input type="checkbox" name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][student_id]['.$ksid.']" checked="checked" value="'. $sid['id'] .'" />';
                                                    } else {
                                                        echo '<input type="checkbox" name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][student_id]['.$ksid.']" value="'. $sid['id'] .'" />';
                                                    }
                                                    echo '</td>';
                                                    echo '<td class="nowrap">'.$sid['name'] . ' (' . $sid['npm'] . ')</td></tr>';
                                                }
                                                echo '</table>';
                                                echo '<span class="label">Centang yang tidak hadir</span>';
                                            endif;
                                        endif;
                                        echo '</td>';
                                        
                                        echo '<td>';
                                        if ($not_user):
                                            echo '&nbsp;';
                                        else:
                                            echo $html->image('cross.png', array(
                                                'alt' => 'Klik untuk menghapus sub kegiatan',
                                                'rel' => 'r'.$key.'_d'.$key2,
                                                'class' => 'del_description'
                                            ));
                                        endif;
                                        echo '</td>';
                                        
                                        echo '<td class="specialCol">';
                                        if ($not_user):
                                            echo $list_activity_description['description'];
                                            // hide it
                                            echo $form->input('description',
                                                array(
                                                    'id' => null,
                                                    'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][description]',
                                                    'class' => 'inputText description',
                                                    'type' => 'hidden', 'div' => false, 'label' => false,
                                                    'value' => $list_activity_description['description']
                                                )
                                            );
                                        else:
                                            echo $form->input('description',
                                                array(
                                                    'id' => null,
                                                    'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][description]',
                                                    'class' => 'inputText description',
                                                    'type' => 'textarea', 'div' => false, 'label' => false,
                                                    'value' => $list_activity_description['description']
                                                )
                                            );
                                        endif;
                                        echo '</td>';
                                        
                                        echo '<td class="specialCol">';
                                        if ($not_user):
                                            echo $list_activity_description['assignment_no'];
                                            // hide it
                                            echo $form->input('assignment_no', array(
                                                'id' => null, 'class' => 'assignment_no', 'type' => 'hidden',
                                                'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][assignment_no]',
                                                'value' => $list_activity_description['assignment_no']
                                            ));
                                        else:
                                            echo $form->input('assignment_no', array(
                                                'div' => false, 'label' => false,
                                                'id' => null, 'class' => 'inputText assignment_no',
                                                'name' => 'data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][assignment_no]',
                                                'value' => $list_activity_description['assignment_no']
                                            ));
                                        endif;
                                        echo '</td>';
                                        echo '<td class="center">';
                                        if ($not_user && !$approver):
                                            // check whether current verficator has the same unit_code_id
                                            // with the study_program_id in description
                                            if ($list_activity_description['StudentClass']['study_program_id'] == $currentUnitCodeId):
                                                echo '<input type="checkbox" value="1"' .
                                                    ' name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][verified]"' .
                                                    ' class="inputText verified"' .
                                                    ($list_activity_description['verified'] ? ' checked="checked"' : '') .
                                                    ' />';
                                            else:
                                                echo $list_activity_description['verified'] ?
                                                    '<span class="green">Sudah diverifikasi prodi bersangkutan</span>' :
                                                    '<span class="red">Akan diverifikasi prodi bersangkutan</span>';
                                                // hide it
                                                echo '<input type="hidden" value="'.$list_activity_description['verified'].'"' .
                                                    ' name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][verified]"' .
                                                    ' class="verified" />';
                                            endif;
                                        else:
                                            if ($approver):
                                                echo '<input type="checkbox" value="1"' .
                                                    ' name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][verified]"' .
                                                    ' class="inputText verified"' .
                                                    ($list_activity_description['verified'] ? ' checked="checked"' : '') .
                                                    ' />';
                                            else:
                                                echo $list_activity_description['verified'] ?
                                                     '<span class="green">Sudah diverifikasi</span>' :
                                                     '<span class="red">Belum diverifikasi</span>';
                                                // hide it
                                                    echo '<input type="hidden" value="'.$list_activity_description['verified'].'"' .
                                                        ' name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][verified]"' .
                                                        ' class="verified" />';
                                            endif;
                                        endif;
                                        echo '</td>';
                                        echo '<td class="center">';
                                        if ($approver):
                                            echo '<input type="checkbox" value="1"' .
                                                ' name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][approved]"' .
                                                ' class="inputText approved"' .
                                                ($list_activity_description['approved'] ? ' checked="checked"' : '') .
                                                ' />';
                                        else:
                                            echo $list_activity_description['approved'] ?
                                                 '<span class="green">Sudah diverifikasi</span>' :
                                                 '<span class="red">Belum diverifikasi</span>';
                                            // hide it
                                            echo '<input type="hidden" value="'.$list_activity_description['approved'].'"' .
                                                    ' name="data[ActList][ActListActivity]['.$key.'][ActListActivityDescription]['.$key2.'][approved]"' .
                                                    ' class="approved" />';
                                        endif;
                                        echo '</td>';
                                        echo '</tr>';
                                    endforeach;
                                } // eof check list_activity_description
                                
                                // add .add_description row (latest row)
                                echo '<tr id="rg'.$key.'" class="add_description_row">' .
                                     '<td colspan="2">&nbsp;</td>';
                                echo '<td colspan="9">';
                                if ($not_user):
                                    echo '&nbsp;';
                                else:
                                    echo '<span class="up"></span>';
                                    echo '<input type="button" class="add_description" id="b'.$key.'" value="+ Tambah sub kegiatan" />';
                                endif;
                                echo '</td></tr>';
                            endforeach; // eof list_activity
                        endif;
                        ?>
                        </tbody>
                        <?php if (!$not_user):?>
                        <tfoot>
                            <tr>
                                <th>&nbsp;</th>
                                <th colspan="10">
                                    <span class="up"></span>
                                    <input type="button" name="add_row" id="add_row" value="+ Tambah Kegiatan" /> &nbsp; atau &nbsp;
                                    <input type="button" name="del_row" id="del_row" value="- Hapus Kegiatan" />
                                </th>
                            </tr>
                        </tfoot>
                        <?php endif;?>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Simpan Aktifitas ini', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action' => $action_back), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
<?php if (isset($action_suffix)): // if we are editing from verify list ?>
<script type="text/javascript">
$(function() {
    var action_suffix = '?<?php echo $action_suffix;?>=1';
    var action = $('#ActListEditForm').attr('action');
    
    $('#ActListEditForm').attr('action', action + action_suffix);
});
</script>
<?php endif;?>
