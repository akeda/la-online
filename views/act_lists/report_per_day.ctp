<?php if ($show_form): ?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ActList', array('action' => 'report_per_day/' . $tpl));?>
	<fieldset>
 		<legend><?php echo 'Rekapitulasi Aktivitas Harian Seluruh Staff';?></legend>
        <table class="input">
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Tanggal');?>:</td>
                <td>
                    <?php
                        echo $form->input('date', array(
                            'label' => false, 'div' => false, 'type' => 'date'
                        ));
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<?php else: ?>
<?php echo $html->css('act_lists_index', 'stylesheet', array('inline' => false, 'media' => 'print, screen'));?>
<h1>Rekapitulasi Aktifitas Staff Tanggal <?php echo $time->format('d/m/Y', $date);?></h1>
<br />
<center>
<table id="tablegrid_act_lists">
    <thead>
        <tr>
            <th>No.</th>
            <th>Nama Staff</th>
            <th>Jabatan</th>
            <th>NIP</th>
            <th>Unit Kerja</th>
            <th>Uraian Kegiatan</th>
            <th>% Kinerja</th>
            <th>Kehadiran</th>
            <th>Potongan Insentif</th>
            <th>Insentif yang diterima</th>
            <th>Diverifikasi oleh</th>
        </tr>
    </thead>
    <tbody>
    <?php if (!empty($records)): ?>
    <?php foreach ($records as $key => $record): ?>
        <tr>
            <td><?php echo $key+1;?></td>
            <td><?php echo $record['CreatedBy']['name'];?></td>
            <td><?php echo $record['CreatedBy']['position_name'];?></td>
            <td><?php echo $record['CreatedBy']['nip'];?></td>
            <td><?php echo $record['CreatedBy']['unit_code_name'];?></td>
            <td><div class="activities"><?php echo $record['ActList']['activity'];?></div></td>
            <td><?php echo $record['ActList']['performance'] . '%';?></td>
            <td><?php echo $record['ActList']['absent'];?></td>
            <td><?php echo 'Rp ' . number_format($record['ActList']['insentive_cutoff'], 2, '.', ',');?></td>
            <td><?php echo 'Rp ' . number_format($record['ActList']['insentive'], 2, '.', ',');?></td>
            <td><?php echo $record['ActList']['verificator'];?></td>
            
        </tr>
    <?php endforeach;?>
    <?php endif;?>
    </tbody>
</table>
<div id="legend" class="left">
    <p>&radic; = <strong>Kegiatan yang terverifikasi</strong></p>
</div>
</center>
<?php endif;?>
