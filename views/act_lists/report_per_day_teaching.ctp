<?php if ($show_form): ?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ActList', array('action' => 'report_per_day/' . $tpl));?>
	<fieldset>
 		<legend><?php echo 'Rekapitulasi Aktivitas Harian Seluruh Staff';?></legend>
        <table class="input">
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Tanggal');?>:</td>
                <td>
                    <?php
                        echo $form->input('date', array(
                            'label' => false, 'div' => false, 'type' => 'date'
                        ));
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<?php else: ?>
<?php echo $html->css('act_lists_index', 'stylesheet', array('inline' => false, 'media' => 'print, screen'));?>
<h2>
DAFTAR HADIR STAFF PENGAJAR<br />
POLITEKNIK MEDIA KREATIF<br />
<?php echo $time->format('d/m/Y', $date);?>
</h2>
<br />
<center>
<table id="tablegrid_act_lists">
    <thead>
        <tr>
            <th>No</th>
            <th>NAMA DOSEN</th>
            <th>MATA KULIAH</th>
            <th>KELAS</th>
            <th>SEMESTER</th>
            <th>WAKTU</th>
            <th>JUMLAH JAM</th>
            <th>VERIFIKASI PRODI</th>
            <th>VERIFIKASI BAAK</th>
        </tr>
    </thead>
    <tbody>
    <?php if (!empty($records)): ?>
    <?php foreach ($records as $key => $record): ?>
        <tr>
            <td><?php echo $key+1;?></td>
            <td><?php echo $record['CreatedBy']['name'];?></td>
            <td><div class="activities"><?php echo $record['ActList']['teaching'];?></div></td>
            <td class="left"><div class="activities"><?php echo $record['ActList']['teaching_classes'];?></div></td>
            <td class="right"><div class="activities"><?php echo $record['ActList']['teaching_semester'];?></div></td>
            <td class="left"><div class="activities"><?php echo $record['ActList']['teaching_time'];?></div></td>
            <td class="right"><div class="activities"><?php echo $record['ActList']['teaching_time_qty'];?></div></td>
            <td class="center"><div class="activities"><?php echo $record['ActList']['teaching_verified'];?></div></td>
            <td class="center"><div class="activities"><?php echo $record['ActList']['teaching_approved'];?></div></td>
        </tr>
    <?php endforeach;?>
    <?php endif;?>
    </tbody>
</table>
</center>
<?php endif;?>
