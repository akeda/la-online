<?php if ($show_form): ?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ActList', array('action' => 'report_per_month/' . $tpl));?>
	<fieldset>
 		<legend><?php echo 'Rekapitulasi Aktivitas Bulanan Staff';?></legend>
        <table class="input">
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Bulan / Tahun');?>:</td>
                <td>
                    <?php
                        echo $form->month('month', date('m'), array(), false);
                        echo '-';
                        echo $form->year('year', 2009, date('Y'), date('Y'));
                    ?>
                </td>
            </tr>
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Nama Staff');?>:</td>
                <td>
                    <?php
                        echo $form->select('user', $users, null, array(
                            'empty' => 'All'
                        ));
                    ?>
                    <br />
                    <span class="label">Pilih All untuk menampilkan semua staff</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<?php else: ?>
<?php echo $html->css('act_lists_index', 'stylesheet', array('inline' => false, 'media' => 'print, screen'));?>
<h1>Rekapitulasi Aktifitas Staff Bulan <?php echo $date;?></h1>
<br />
<?php if (isset($user)): ?>
<div class="left" style="float: left">
    <table class="noborder">
        <tr>
            <td class="label">Nama Staff:</td>
            <td class="val"><?php echo $user['CreatedBy']['name'];?></td>
        </tr>
        <tr>
            <td class="label">Jabatan:</td>
            <td class="val"><?php echo $user['Position']['name'];?></td>
        </tr>
        <tr>
            <td class="label">NIP:</td>
            <td class="val"><?php echo $user['CreatedBy']['nip'];?></td>
        </tr>
        <tr>
            <td class="label">Unit Kerja:</td>
            <td class="val"><?php echo $user['UnitCode']['name'];?></td>
        </tr>
        <tr>
            <td class="label">Total % kinerja:</td>
            <td class="val"><?php echo number_format($total_perfomance, 2, '.', ',');?> %</td>
        </tr>
        <tr>
            <td class="label">Total insentif:</td>
            <td class="val">Rp <?php echo number_format($total_insentive, 2, '.', ',');?></td>
        </tr>
    </table>
</div>
<div class="right" style="float: right">
    <table class="noborder">
        <?php foreach ($absent_options as $absent_key => $absent_nice_name): ?>
        <tr>
            <td class="label"><?php echo $absent_nice_name;?>:</td>
            <td class="val"><?php echo $total_hadir[$absent_key];?></td>
        </tr>
        <?php endforeach;?>
    </table>
</div>
<br style="clear: both;" />
<br />

<?php endif;?>
<center>
<table id="tablegrid_act_lists">
    <thead>
        <tr>
        <?php if (isset($user)): ?>
            <th>No.</th>
            <th>Tanggal Aktifitas</th>
            <th>Uraian Kegiatan</th>
            <th>% Kinerja</th>
            <th>Kehadiran</th>
            <th>Potongan Insentif</th>
            <th>Insentif yang diterima</th>
            <th>Diverifikasi oleh</th>
        <?php else: ?>
            <th>No.</th>
            <th>Nama Staff</th>
            <th>Jabatan</th>
            <th>NIP</th>
            <th>Unit Kerja</th>
            <th>Tanggal Aktifitas</th>
            <th>Uraian Kegiatan</th>
            <th>% Kinerja</th>
            <th>Kehadiran</th>
            <th>Potongan Insentif</th>
            <th>Insentif yang diterima</th>
            <th>Diverifikasi oleh</th>
        <?php endif;?>
        </tr>
    </thead>
    <tbody>
    <?php if (!empty($records)): ?>
    <?php foreach ($records as $key => $record): ?>
        <tr>
            <?php if (isset($user)): ?>
                <td><?php echo $key+1;?></td>
                <td><?php echo $time->format('d/m/Y', $record['ActList']['activity_date']);?></td>
                <td><div class="activities"><?php echo $record['ActList']['activity'];?></div></td>
                <td><?php echo $record['ActList']['performance'] . '%';?></td>
                <td><?php echo $record['ActList']['absent'];?></td>
                <td><?php echo 'Rp ' . number_format($record['ActList']['insentive_cutoff'], 2, '.', ',');?></td>
                <td><?php echo 'Rp ' . number_format($record['ActList']['insentive'], 2, '.', ',');?></td>
                <td><?php echo $record['ActList']['verificator'];?></td>

            <?php else: ?>
                <td><?php echo $key+1;?></td>
                <td><?php echo $record['CreatedBy']['name'];?></td>
                <td><?php echo $record['CreatedBy']['position_name'];?></td>
                <td><?php echo $record['CreatedBy']['nip'];?></td>
                <td><?php echo $record['CreatedBy']['unit_code_name'];?></td>
                <td><?php echo $time->format('d/m/Y', $record['ActList']['activity_date']);?></td>
                <td><div class="activities"><?php echo $record['ActList']['activity'];?></div></td>
                <td><?php echo $record['ActList']['performance'] . '%';?></td>
                <td><?php echo $record['ActList']['absent'];?></td>
                <td><?php echo 'Rp ' . number_format($record['ActList']['insentive_cutoff'], 2, '.', ',');?></td>
                <td><?php echo 'Rp ' . number_format($record['ActList']['insentive'], 2, '.', ',');?></td>
                <td><?php echo $record['ActList']['verificator'];?></td>
            <?php endif;?>
        </tr>
    <?php endforeach;?>
    <?php endif;?>
    </tbody>
</table>
<div id="legend" class="left">
    <p>&radic; = <strong>Kegiatan yang terverifikasi</strong></p>
</div>
</center>
<?php endif;?>
