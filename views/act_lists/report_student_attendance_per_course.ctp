<?php if ($show_form): ?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ActList', array('action' => 'report_student_attendance/' . $tpl));?>
	<fieldset>
 		<legend><?php echo 'Rekapitulasi Kehadiran dan Ketidakhadiran Mahasiswa';?></legend>
        <table class="input">
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Tanggal');?>:</td>
                <td>
                    <?php
                        echo $form->input('date', array(
                            'label' => false, 'div' => false, 'type' => 'date',
                            'dateFormat' => 'MY'
                        ));
                    ?>
                </td>
            </tr>
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Kelas');?>:</td>
                <td>
                    <?php
                        echo $form->select('class', $student_classes, null, array(
                            'empty' => false, 'id' => 'class'
                        ));
                    ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Mata Kuliah');?>:</td>
                <td>
                    <?php
                        echo $form->select('course', array(), null, array(
                            'empty' => false, 'id' => 'course'
                        ));
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<script type="text/javascript">
var get_courses_url = '<?php echo $get_courses_url;?>';
$(function() {
    $('#class').change(function() {
        get_courses( this.value );
    }).trigger('change');

    function get_courses(class_id) {
        $.get(get_courses_url + class_id, function(resp) {
            $('#course').html(resp);
        });
    }
});
</script>
<?php else: ?>
<?php echo $html->css('act_lists_index', 'stylesheet', array('inline' => false, 'media' => 'print, screen'));?>
<h2>
REKAPITULASI KEHADIRAN DAN KETIDAKHADIRAN MAHASISWA<br />
POLITEKNIK MEDIA KREATIF<br />
</h2>
<center>
<table id="tablegrid_act_lists">
    <thead>
        <tr>
            <th colspan="30" class="noborder">
            <table class="noborder">
                <tr>
                    <td class="label">Mata Kuliah:</td>
                    <td class="val"><?php echo $course;?></td>
                </tr>
                <tr>
                    <td class="label">Bulan/Tahun:</td>
                    <td class="val"><?php echo $date;?></td>
                </tr>
                <tr>
                    <td class="label">Kelas:</td>
                    <td class="val"><?php echo $class;?></td>
                </tr>
                <tr>
                    <td class="label">Semester:</td>
                    <td class="val"><?php echo $semester;?></td>
                </tr>
                <tr>
                    <td class="label">Angkatan:</td>
                    <td class="val"><?php echo $year_started;?></td>
                </tr>
            </table>
            </th>
        </tr>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">NIM</th>
            <th rowspan="2">Nama Mahasiswa</th>
            <?php for ($i = 0; $i < 5; $i++):?>
            <th colspan="5" class="center">Minggu <?php echo $i+1;?></th>
            <?php endfor;?>
            <th rowspan="2">Jumlah hadir</th>
            <th rowspan="2">Jumlah tidak hadir</th>
        </tr>
        <tr>
            <?php for ($i = 0; $i < 25; $i++):?>
                <td><?php echo $week_name[$i%5];?></td>
            <?php endfor;?>
        </tr>
    </thead>
    <tbody>
    <?php if (!empty($students)): ?>
        <?php foreach ($students as $key => $record): ?>
            <tr>
                <td><?php echo $key+1;?></td>
                <td><?php echo $record['Student']['npm'];?></td>
                <td><?php echo $record['Student']['name'];?></td>
                <?php for ($i = 0; $i < 5; $i++):?>
                    <?php for ($j = 1; $j < 6; $j++):?>
                        <td>
                        <?php
                            echo $record['Student']['attendance'][$i][$j] ?
                                 $record['Student']['attendance'][$i][$j] : '';
                        ?>
                        </td>
                    <?php endfor;?>
                <?php endfor;?>
                <td class="right">
                <?php
                    echo $total_in[$record['Student']['id']] ?
                         $total_in[$record['Student']['id']] : '';
                ?>
                </td>
                <td class="right">
                <?php
                    echo $total_absent[$record['Student']['id']] ?
                         $total_absent[$record['Student']['id']] : '';
                ?>
                </td>
            </tr>
        <?php endforeach;?>
    <?php endif;?>
    </tbody>
</table>
</center>
<?php endif;?>
