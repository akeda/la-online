<?php if ($show_form): ?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ActList', array('action' => 'report_per_day/' . $tpl));?>
	<fieldset>
 		<legend><?php echo 'Rekapitulasi Kehadiran Mengajar Bulanan';?></legend>
        <table class="input">
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Tanggal');?>:</td>
                <td>
                    <?php
                        echo $form->input('date', array(
                            'label' => false, 'div' => false, 'type' => 'date',
                            'dateFormat' => 'MY'
                        ));
                    ?>
                </td>
            </tr>
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Nama Staff');?>:</td>
                <td>
                    <?php
                        echo $form->select('user', $users, null, array(
                            'empty' => 'All'
                        ));
                    ?>
                    <br />
                    <span class="label">Pilih All untuk menampilkan semua staff</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<?php else: ?>
<?php echo $html->css('act_lists_index', 'stylesheet', array('inline' => false, 'media' => 'print, screen'));?>
<h2>
REKAPITULASI KEHADIRAN MENGAJAR<br />
POLITEKNIK MEDIA KREATIF<br />
<?php echo $date;?>
</h2>
<center>
<table id="tablegrid_act_lists">
    <thead>
        <tr>
        <?php if (isset($user)): ?>
            <th colspan="9" class="noborder">
            <table class="noborder">
                <tr>
                    <td class="label">Nama Staff:</td>
                    <td class="val"><?php echo $user['CreatedBy']['name'];?></td>
                </tr>
                <tr>
                    <td class="label">Jabatan:</td>
                    <td class="val"><?php echo $user['Position']['name'];?></td>
                </tr>
                <tr>
                    <td class="label">NIP:</td>
                    <td class="val"><?php echo $user['CreatedBy']['nip'];?></td>
                </tr>
            </table>
            </th>
        </tr>
        <tr>
            <th>No</th>
            <th>Tanggal</th>
            <th>MATA KULIAH</th>
            <th>POKOK BAHASAN</th>
            <th>KELAS</th>
            <th>SEMESTER</th>
            <th>WAKTU</th>
            <th>JUMLAH JAM</th>
            <th>VERIFIKASI PRODI</th>
            <th>VERIFIKASI BAAK</th>
        <?php else: ?>
            <th>No</th>
            <th>Tanggal</th>
            <th>NAMA DOSEN</th>
            <th>MATA KULIAH</th>
            <th>POKOK BAHASAN</th>
            <th>KELAS</th>
            <th>SEMESTER</th>
            <th>WAKTU</th>
            <th>JUMLAH JAM</th>
            <th>VERIFIKASI PRODI</th>
            <th>VERIFIKASI BAAK</th>
        <?php endif;?>
        </tr>
    </thead>
    <tbody>
    <?php if (!empty($records)): ?>
        <?php if (isset($user)): ?>
            <?php foreach ($records as $key => $record): ?>
                <tr>
                    <td><?php echo $key+1;?></td>
                    <td><?php echo $time->format('d/m/Y', $record['ActList']['activity_date']);?></td>
                    <td><div class="activities"><?php echo $record['ActList']['teaching'];?></div></td>
                    <td><div class="activities"><?php echo $record['ActList']['teaching_materials'];?></div></td>
                    <td class="left"><div class="activities"><?php echo $record['ActList']['teaching_classes'];?></div></td>
                    <td class="right"><div class="activities"><?php echo $record['ActList']['teaching_semester'];?></div></td>
                    <td class="left"><div class="activities"><?php echo $record['ActList']['teaching_time'];?></div></td>
                    <td class="right"><div class="activities"><?php echo $record['ActList']['teaching_time_qty'];?></div></td>
                    <td class="center"><div class="activities"><?php echo $record['ActList']['teaching_verified'];?></div></td>
                    <td class="center"><div class="activities"><?php echo $record['ActList']['teaching_approved'];?></div></td>
                </tr>
            <?php endforeach;?>
        <?php else: ?>
            <?php foreach ($records as $key => $record): ?>
                <tr>
                    <td><?php echo $key+1;?></td>
                    <td><?php echo $time->format('d/m/Y', $record['ActList']['activity_date']);?></td>
                    <td><?php echo $record['CreatedBy']['name'];?></td>
                    <td><div class="activities"><?php echo $record['ActList']['teaching'];?></div></td>
                    <td><div class="activities"><?php echo $record['ActList']['teaching_materials'];?></div></td>
                    <td class="left"><div class="activities"><?php echo $record['ActList']['teaching_classes'];?></div></td>
                    <td class="right"><div class="activities"><?php echo $record['ActList']['teaching_semester'];?></div></td>
                    <td class="left"><div class="activities"><?php echo $record['ActList']['teaching_time'];?></div></td>
                    <td class="right"><div class="activities"><?php echo $record['ActList']['teaching_time_qty'];?></div></td>
                    <td class="center"><div class="activities"><?php echo $record['ActList']['teaching_verified'];?></div></td>
                    <td class="center"><div class="activities"><?php echo $record['ActList']['teaching_approved'];?></div></td>
                </tr>
            <?php endforeach;?>
        <?php endif;?>
    <?php endif;?>
    </tbody>
</table>
</center>
<?php endif;?>
