<?php if ( !empty($courses) ):?>
    <?php foreach ($courses as $course):?>
        <?php echo '<option value="' . $course['Course']['id'] . '">' . $course['Course']['name'] . '</option>';?>
    <?php endforeach;?>
<?php else:?>
    <?php echo '<option value=""></option>';?>
<?php endif;?>
