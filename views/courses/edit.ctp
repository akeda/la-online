<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Course');?>
	<fieldset>
 		<legend>Tambah mata kuliah</legend>
        <table class="input">
            <tr>
                <td class="label-required">Nama Prodi</td>
                <td>
                <?php 
                    echo $form->select('study_program_id', $study_programs, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required'
                        ), true);
                    echo ($form->isFieldError('study_program_id')) ? $form->error('study_program_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nama mata kuliah</td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required">Semester</td>
                <td>
                <?php
                    echo $form->select('semester', $semesters, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required'
                    ), false);
                    echo ($form->isFieldError('semester')) ? $form->error('semester') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Tahun ajaran</td>
                <td><?php echo $form->input('year_study', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required">SKS</td>
                <td><?php echo $form->input('sks', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Update', true), array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['Course']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['Course']['name'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
