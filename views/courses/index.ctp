<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array(
            "fields" => array(
                "name" => array(
                    'title' => 'Nama mata kuliah',
                    'sortable' => true
                ),
                'year_study' => array(
                    'title' => 'Tahun ajaran',
                    'sortable' => true
                ),
                'semester' => array(
                    'title' => 'Semester',
                    'sortable' => true
                ),
                "sks" => array(
                    'title' => 'SKS',
                    'sortable' => true
                ),
                "study_program_id" => array(
                    'title' => 'Prodi',
                    'sortable' => true
                )
            ),
            "editable"  => "name",
            "assoc" => array(
                "study_program_id" => array(
                    'model' => 'StudyProgram',
                    'field' => 'name'
                )
            )
        ));
?>
</div>
