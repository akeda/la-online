<?php
class CoursesController extends AppController {
    var $pageTitle = 'Mata Kuliah';
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function edit($id = null) {
        $this->__setAdditionals();
        parent::edit($id); 
    }
    
    function __setAdditionals() {
        $study_programs = $this->Course->StudyProgram->find('list');
        $this->set('study_programs', $study_programs);
        
        $semesters = array(
            '1' => 'I',
            '2' => 'II',
            '3' => 'III',
            '4' => 'IV',
            '5' => 'V',
            '6' => 'VI',
            '7' => 'VII',
            '8' => 'VIII',
            '9' => 'IX',
            '10' => 'X',
        );
        $this->set('semesters', $semesters);
    }
    
    function getCourses($student_class_id) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';
        
        App::import('Model', 'StudentClass');
        $StudentClass = new StudentClass;
        $student_class = $StudentClass->find('first', array(
            'conditions' => array('id' => $student_class_id),
            'recursive' => -1
        ));
        $study_program_id = $student_class['StudentClass']['study_program_id'];
        
        $courses = $this->Course->find('all', array(
            'conditions' => array(
                'study_program_id' => $study_program_id
            ),
            'recursive' => -1, 'order' => 'name ASC'
        ));
        
        $this->set('courses', $courses);
    }
}
?>
