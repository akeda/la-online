<?php
class StudentPaymentObligationsController extends AppController {
    var $pageTitle = 'Besar SPP dan DPP per angkatan';

    function index() {
        $this->paginate['order'] = 'year_started ASC';
        parent::index();
    }
}
