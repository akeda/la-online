<?php
class DupaksController extends AppController {
    var $pageTitle = 'Daftar Usul Penetapan Angka Kredit (DUPAK)';
    
    function index() {
        parent::index();
    }
    
    function add() {
        $this->__ajaxURL();
        parent::add();
    }
    
    function edit($id) {
        $this->__ajaxURL();
        $this->Dupak->Behaviors->attach('Containable');
        $created_by = $this->Dupak->find('first', array(
            'conditions' => array(
                'Dupak.id' => $id
            ),
            'contain' => array(
                'CreatedBy' => array(
                    'fields' => array('name', 'nip')
                )
            )
        ));
        $this->set('created_by', $created_by['CreatedBy']);
        parent::edit($id);
    }
    
    function __ajaxURL() {
        $this->set('ajaxURL', $this->__pathToController() . '/is_dupak_exists');
    }
    
    function printhtml() {
        $show_form = false;
        if ( !empty($this->data['y']['year']) && !empty($this->data['Dupak']['user']) ) {
            $this->layout = 'printhtml';
            Configure::write('debug', 0);

            $user_id = $this->data['Dupak']['user'];
            $r = $this->Dupak->find('first', array(
                'conditions' => array(
                    'Dupak.created_by' => $user_id,
                    'dupak_year' => $this->data['y']['year']
                )
            ));
            $this->set('r', $r);
        } else {
            $show_form = true;
            $this->set('users', $this->Dupak->CreatedBy->find('list', array(
                'order' => 'name ASC',
                'recursive' => -1
            )));
        }
        
        $this->set('show_form', $show_form);
    }
    
    function is_dupak_exists($dupak_year) {
        $this->layout = 'ajax';
        Configure::write('debug', 0);
        
        $r = '';
        $r = $this->Dupak->find('count', array(
            'conditions' => array(
                'Dupak.dupak_year' => $dupak_year,
                'Dupak.created_by' => $this->Auth->user('id')
            )
        ));
        $this->set('r', $r);
    }
}
