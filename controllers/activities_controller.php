<?php
class ActivitiesController extends AppController {
    var $pageTitle = 'Kegiatan';
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function edit($id) {
        $this->__setAdditionals();
        parent::edit($id);
    }
    
    function __setAdditionals() {
        $positions = $this->Activity->Position->generatetreelist();
        $this->set('positions', $positions);
    }
}
?>
