<?php
class StudentsController extends AppController {
    var $pageTitle = 'Mahasiswa';
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function edit($id = null) {
        $this->__setAdditionals();
        parent::edit($id); 
    }
    
    function __setAdditionals() {
        $study_programs = $this->StudentClass->StudyProgram->find('list');
        $this->set('study_programs', $study_programs);
    }
}
?>
