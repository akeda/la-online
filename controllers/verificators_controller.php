<?php
class VerficatorsController extends AppController {
    var $pageTitle = 'Verifikator';
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function edit($id) {
        $this->__setAdditionals();
        parent::edit($id);
    }
    
    function __setAdditionals() {
        $positions = $this->Verificator->Position->find('list');
        $this->set('positions', $positions);
    }
}
?>
