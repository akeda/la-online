<?php
class PositionsController extends AppController {
    var $pageTitle = 'Jabatan';
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function edit($id) {
        $this->__setAdditionals($id);
        parent::edit($id);
    }
    
    function __setAdditionals($id = null) {
        $conditions = array('Position.id <>' => $id);
        $positions = $this->Position->generatetreelist($conditions, null, null, '&nbsp;&nbsp;&nbsp;');
        $this->set('positions', $positions);
    }
}
?>
