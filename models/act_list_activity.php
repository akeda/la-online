<?php
class ActListActivity extends AppModel {
    var $validate = array(
        'activity_id' => array(
            'rule' => 'vActivity',
            'message' => 'Invalid Aktifitas'
        )
    );
    
    var $belongsTo = array(
        'ActList', 'Activity'
    );
    
    var $hasMany = array(
        'ActListActivityDescription' => array(
            'dependent' => true
        )
    );
    
    function vActivity($field) {
        return $this->Activity->find('count', array(
            'conditions' => array(
                'Activity.id' => $field["activity_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
}
?>
