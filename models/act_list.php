<?php
class ActList extends AppModel {
    const ABSENT_IZIN  = 1;
    const ABSENT_SAKIT = 2;
    const ABSENT_CUTI  = 3;
    const ABSENT_TK    = 4; // tidak kerja
    const ABSENT_DL    = 5; // diklat
    const ABSENT_HADIR = 6; // kerja
    const ABSENT_DLU   = 7; // dinas luar
    
    var $validate = array(
        'absent' => array(
            'rule' => array('inList', array(1,2,3,4,5,6,7)),
            'message' => 'Masukkan sesuai pilihan yang tersedia'
        ),
        /*
        'verified_by' => array(
            'rule' => 'vVerifiedBy',
            'message' => 'Verifikator tidak valid'
        )*/
    );
    
    var $belongsTo = array(
        'VerifiedBy' => array(
            'className' => 'User',
            'foreignKey' => 'verified_by'
        ),
        'CreatedBy' => array(
            'className' => 'User',
            'foreignKey' => 'created_by'
        ),
    );
    
    var $hasMany = array(
        'ActListActivity' => array(
            'dependent' => true
        )
    );
    
    var $_paginateCounter;
    
    function generateAbsentOptions() {
        $options = array();
        $options[ self::ABSENT_HADIR ]  = 'Hadir';
        $options[ self::ABSENT_IZIN ]  = 'Izin';
        $options[ self::ABSENT_SAKIT ]  = 'Sakit';
        $options[ self::ABSENT_CUTI ]  = 'Cuti';
        $options[ self::ABSENT_TK ]  = 'Tidak Kerja';
        $options[ self::ABSENT_DL ]  = 'Mengikuti Diklat';
        $options[ self::ABSENT_DLU ]  = 'Dinas Luar';
        
        return $options;
    }
    
    function colorizeAbsent($absent) {
        $ret = '';
        // colorize absent
        switch ($absent) {
            case self::ABSENT_IZIN:
                $ret = '<span class="yellow"><strong>Hadir</strong></span>';
                break;
            case self::ABSENT_SAKIT:
                $ret = '<span class="yellow"><strong>Izin</strong></span>';
                break;
            case self::ABSENT_CUTI:
                $ret = '<span class="yellow"><strong>Cuti</strong></span>';
                break;
            case self::ABSENT_TK:
                $ret = '<span class="red"><strong>Tidak Kerja</strong></span>';
                break;
            case self::ABSENT_DL:
                $ret = '<span class="green"><strong>Mengikuti Diklat</strong></span>';
                break;
            case self::ABSENT_DLU:
                $ret = '<span class="green"><strong>Dinas Luar</strong></span>';
                break;
            case self::ABSENT_HADIR:
                $ret = '<span class="green"><strong>Hadir</strong></span>';
                break;
        }
        
        return $ret;
    }
    
    function getApprover() {
        $approver = null;
        
        $_approver = $this->CreatedBy->Position->find('first', array(
            'conditions' => array(
                'special' => 1
            )
        ));
        if ( isset($_approver['Position']['id']) ) {
            $approver = $_approver['Position'];
        }
        
        return $approver;
    }
    
    function getVerificator($user_id) {
        $user_position = $this->CreatedBy->find('first', array(
            'conditions' => array('id' => $user_id),
            'fields' => array('position_id'),
            'recursive' => -1
        ));
        
        $verificator = null;
        $parnode = $this->CreatedBy->Position->getparentnode($user_position['CreatedBy']['position_id']);
        if ( isset( $parnode['Position']['id'] ) && $parnode['Position']['id'] ) {
            $verificator = $parnode['Position'];
        }
        
        return $verificator;
    }
    
    function vVerifiedBy($field) {
        // not verfied yet
        if ( !isset($field['verified_by']) || empty($field['verified_by']) ) {
            return true;
        }
        
        $this->CreatedBy->Behaviors->attach('Containable');
        
        $user_id = $this->data['Auth']['User']['id'];
        $user_position = $this->CreatedBy->find('first', array(
            'conditions' => array('id' => $user_id),
            'fields' => array('position_id'),
            'recursive' => -1
        ));
        
        $verificator_id = $field['verified_by'];
        $parnode = $this->CreatedBy->Position->getparentnode($user_position['CreatedBy']['position_id']);
        $parent_id = null;
        if ( isset( $parnode['Position']['id'] ) && $parnode['Position']['id'] ) {
            $parent_id = $parnode['Position']['id'];
        }
        
        if ( $parent_id == $verificator_id ) {
            return true;
        }
        
        return false;
    }
/*
    function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
        $this->Behaviors->attach('Containable');
        $fields = array('id', 'activity_date', 'absent', 'approved', 'approved_by',
            'verified', 'verified_by', 'created', 'created_by'
        );
        $positions = $this->VerifiedBy->Position->find('list');
        $contain = array(
            'CreatedBy' => array(
                'fields' => array('name', 'nip'),
                'Position' => array(
                    'fields' => array('name', 'parent_id')
                ),
                'UnitCode' => array(
                    'fields' => array('name')
                )
            ),
            'ActListActivity' => array(
                'Activity' => array(
                    'fields' => array('name')
                ),
                'ActListActivityDescription' => array(
                    'StudentClass', 'Course'
                )
            )
        );
        $recursive = 3;
        
        if ( isset($conditions['ActList.currentUnitCodeId']) && $conditions['ActList.currentUnitCodeId'] ) {
            $currentUnitCodeId = $conditions['ActList.currentUnitCodeId'];
            unset($conditions['ActList.currentUnitCodeId']);
        }

        $records = $this->find('all', compact(
            'conditions', 'fields', 'order', 'limit',
            'page', 'recursive', 'group', 'contain'
            )
        );
        $absent_options = $this->generateAbsentOptions();
        
        foreach ( $records as $key => $record ) {
            $records[$key]['CreatedBy']['unit_code_name'] = isset($record['CreatedBy']['UnitCode']['name']) ?
                                                            $record['CreatedBy']['UnitCode']['name'] : '';
            $records[$key]['CreatedBy']['position_name'] = isset($record['CreatedBy']['Position']['name']) ?
                                                           $record['CreatedBy']['Position']['name'] : '';
            $records[$key]['ActList']['activity'] = '';
            if ( isset($positions[$records[$key]['ActList']['verified_by']]) ) {
                $records[$key]['ActList']['verificator'] = $positions[$records[$key]['ActList']['verified_by']];
            }
            if ( !empty($record['ActList']['verified']) ) {
                $records[$key]['ActList']['verified'] = '<span class="green"><strong>' .
                    date('d/m/Y H:i:s', strtotime($record['ActList']['verified'])) . '</strong></span>';
            }
            if ( !empty($record['ActList']['approved']) ) {
                $records[$key]['ActList']['approved'] = '<span class="green"><strong>' .
                    date('d/m/Y H:i:s', strtotime($record['ActList']['approved'])) . '</strong></span>';
            }
            
            // colorize absent
            $records[$key]['ActList']['absent'] = $this->colorizeAbsent($records[$key]['ActList']['absent']);
            
            if ( !empty($record['ActListActivity']) ) {
                $records[$key]['ActList']['activity'] = '<ol>';
                foreach ( $record['ActListActivity'] as $key2 => $activity ) {
                    $records[$key]['ActList']['study_programs'] = array();
                    $records[$key]['ActList']['activity'] .= '<li>' . $activity['Activity']['name'];
                    if ( !empty($activity['ActListActivityDescription']) ) {
                        $records[$key]['ActList']['activity'] .= '<ol>';
                        foreach ( $activity['ActListActivityDescription'] as $key3 => $desc ) {
                            //if ( !empty($desc['description']) ) {
                                $records[$key]['ActList']['activity'] .=  '<li>';
                                
                                if ( !empty($desc['StudentClass']) ) {
                                    $records[$key]['ActList']['activity'] .= $desc['StudentClass']['name'] . ' (Angkatan ' . $desc['StudentClass']['year_started'] . '). ';
                                    $records[$key]['ActList']['study_programs'][$desc['StudentClass']['study_program_id']] =
                                        $desc['StudentClass']['study_program_id'];
                                }
                                if ( !empty($desc['Course']) ) {
                                    $records[$key]['ActList']['activity'] .= ' Mata kuliah ' . $desc['Course']['name'] . ' (' . $desc['Course']['sks'] . ' SKS)<br />';
                                }
                                if ( !empty($desc['start_hour']) && !empty($desc['end_hour']) ) {
                                    $records[$key]['ActList']['activity'] .= $desc['start_hour'] . ':' . $desc['start_minute'] . ' s/d ' .
                                                                             $desc['end_hour'] . ':' . $desc['end_minute'] . '<br />';
                                }
                                
                                $records[$key]['ActList']['activity'] .=  $desc['description'];
                                
                                if ( !empty($desc['result']) ) {
                                    $records[$key]['ActList']['activity'] .= ' &rarr; <strong>' . $desc['result'] .
                                        '</strong>';
                                }
                                
                                $records[$key]['ActList']['activity'] .=  '</li>';
                            //}
                        }
                        $records[$key]['ActList']['activity'] .= '</ol>';
                    }
                    $records[$key]['ActList']['activity'] .= '</li>';
                }
                $records[$key]['ActList']['activity'] .= '</ol>';
            }
        }
        
        if ( isset($currentUnitCodeId) && $currentUnitCodeId ) {
            foreach ($records as $key => $record) {
                if ( !in_array($currentUnitCodeId, $record['ActList']['study_programs']) ) {
                    unset($records[$key]);
                }
            }
        }
        
        $this->_paginateCounter = count($records);
        
        return $records;
    }

    /**
     * Overridden paginateCount method
     */ /*
    function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $currentUnitCodeId = array();
        if ( isset($conditions['ActList.currentUnitCodeId']) && $conditions['ActList.currentUnitCodeId'] ) {
            $currentUnitCodeId = $conditions['ActList.currentUnitCodeId'];
            unset($conditions['ActList.currentUnitCodeId']);
        }

        $contain = array(
            'CreatedBy' => array(
                'fields' => array('name', 'nip'),
                'Position' => array(
                    'fields' => array('name', 'parent_id')
                ),
                'UnitCode' => array(
                    'fields' => array('name')
                )
            ),
            'ActListActivity' => array(
                'Activity' => array(
                    'fields' => array('name')
                ),
                'ActListActivityDescription' => array(
                    'StudentClass' => array('fields' => 'study_program_id')
                )
            )
        );
        $recursive = 3;
        
        $records = $this->find('all', compact(
            'conditions', 'fields', 'order', 'limit',
            'page', 'recursive', 'group', 'contain'
            )
        );
        
        foreach ( $records as $key => $record ) {
            $records[$key]['ActList']['study_programs'] = array();
            foreach ( $record['ActListActivity'] as $key2 => $activity ) {
                foreach ( $activity['ActListActivityDescription'] as $key3 => $desc ) {
                    if ( isset($desc['StudentClass']['study_program_id']) ) {
                        $records[$key]['ActList']['study_programs'][$desc['StudentClass']['study_program_id']] =
                                        $desc['StudentClass']['study_program_id'];
                    }
                }
            }
            
            if ( $currentUnitCodeId &&
                 !in_array($currentUnitCodeId, $records[$key]['ActList']['study_programs']) )
            {
                unset($records[$key]);
            }
        }
        
        return count($records);
    }
*/
}

