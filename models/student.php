<?php
class Student extends AppModel {
    var $validate = array(
        'student_class_id' => array(
            'rule' => 'vStudentClass',
            'message' => 'Pilih Kelas'
        ),
        'name' => array(
            'rule' => 'notEmpty',
            'message' => 'Isi Nama'
        ),
        'nim' => array(
            'rule' => 'notEmpty',
            'message' => 'Isi NIM'
        ),
    );

    var $belongsTo = array('StudentClass');
    var $hasMany = array('StudentPayment');

    function vStudentClass($field) {
        return $this->StudentClass->find('count', array(
            'conditions' => array(
                'StudentClass.id' => $field["student_class_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
}
?>
