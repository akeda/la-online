<?php
class StudentClass extends AppModel {
    var $validate = array(
        'study_program_id' => array(
            'rule' => 'vStudyProgram',
            'message' => 'Pilih Nama Prodi'
        ),
        'name' => array(
            'rule' => 'notEmpty',
            'message' => 'Isi Nama kelas'
        ),
        'year_started' => array(
            'rule' => 'numeric',
            'message' => 'Isi Tahun masuk'
        )
    );

    var $belongsTo = array('StudyProgram');
    var $hasMany = array(
        'Student' => array(
            'dependent' => true
        )
    );

    function vStudyProgram($field) {
        return $this->StudyProgram->find('count', array(
            'conditions' => array(
                'StudyProgram.id' => $field["study_program_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
}
?>
