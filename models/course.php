<?php
class Course extends AppModel {
    var $validate = array(
        'study_program_id' => array(
            'rule' => array('vStudyProgram'),
            'message' => 'Choose based on available option'
        ),
        'name' => array(
            'required' => array(
                'allowEmpty' => false,
                'rule' => 'notEmpty',
                'message' => 'This field cannot be empty'
            ),
            'maxlength' => array(
                'rule' => array('maxlength', 100),
                'message' => 'Maximum character is 100 characters'
            ),
        )
    );
    var $belongsTo = array('StudyProgram');
    
/**
 * Methods with v prefix are custom validation
 * rule
 */
    function vStudyProgram($field) {
        return $this->StudyProgram->find('count', array(
            'conditions' => array(
                'StudyProgram.id' => $field["study_program_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
}
?>
