<?php
class Verificator extends AppModel {
    var $validate = array(
        'user_id' => array(
            'valid' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => 'vUser',
                'message' => 'Pengguna tidak terdaftar'
            )
        )
    );
    
    var $belongsTo = array(
        'Verifier' => array(
            'className' => 'Position',
            'foreignKey' => 'verificator'
        ),
        'Position' => array(
            'className' => 'Position',
            'foreignKey' => 'position_id'
        ),
        'CreatedBy' => array(
            'className' => 'User',
            'foreignKey' => 'created_by'
        )
    );
    
    function vUser($field) {
        return $this->User->find('count', array(
            'conditions' => array(
                'User.id' => $field["user_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
}
?>
