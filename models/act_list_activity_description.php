<?php
class ActListActivityDescription extends AppModel {
    var $belongsTo = array(
        'ActListActivity', 'StudentClass', 'Course'
    );
    
    var $hasMany = array(
        'ActListActivityDescriptionStudent' => array(
            'dependent' => true
        )
    );
    
    function afterSave($created) {
        // save all student absents
        if ( $created ) {
            if ( !empty($this->data['ActListActivityDescription']['student_id']) ) {
                
                $data = array();
                foreach ($this->data['ActListActivityDescription']['student_id'] as $key => $sid) {
                    $data[$key] = array(
                        'act_list_activity_description_id' => $this->id,
                        'student_id' => $sid
                    );
                }
                $this->ActListActivityDescriptionStudent->create();
                $this->ActListActivityDescriptionStudent->saveAll($data);
            }
        }
    }
}
?>
