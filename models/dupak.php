<?php
class Dupak extends AppModel {
    var $belongsTo = array(
        'CreatedBy' => array(
            'foreignKey' => 'created_by',
            'className' => 'User'
        )
    );
    
    function beforeSave($created) {
        if ( $created ) {
            $this->data['Dupak']['dupak_year'] = date('Y');
        }
        return true;
    }
}
