<?php
class StudentPaymentObligation extends AppModel {
    var $validate = array(
        'year_started' => array(
            'rule' => array('unique', 'year_started'),
            'message' => 'SPP dan DPP angkatan ini sudah di input, harap pilih edit'
        ),
        'spp' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'notEmpty',
            'message' => 'Wajib diisi'
        ),
        'dpp' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'notEmpty',
            'message' => 'Wajib diisi'
        )
    );
}
