var trClone; // first loaded tr is cloned
$(function() {
    var tableScope = $('#payments');
    var tableScopeBody = $('tbody:first', tableScope);
    
    // add row
    $('#add_row').live("click", function(e) {
        var lastRow = $('tr.row_payment:last', tableScope);
        
        // make sure exists one row on tableScope
        if ( lastRow.length ) {
            // preformating the clone
            newRow = preformatNewRow(lastRow.clone());
            
            // cache for later use
            if ( typeof(trClone) == 'undefined' ) {
                trClone = newRow;
            }

            lastRow.after(newRow);
        }
    });
    
    // delete row
    $('#del_row').click(function(e) {
        var totalCb = $('.cb_payment').length;
        var totalChecked = $('.cb_payment:checked').length;
        
        if ( totalChecked == 0 ) {
            alert('Centang pembayaran yang ingin Anda hapus pada kotak\n'+
                  'kecil sebelah kiri semester'
            );
            return false;
        }
        
        if ( totalChecked < totalCb ) {
            $('.cb_payments:checked').each(function(idx) {
                var rowId = $(this).parent().parent().attr('id');
                $('tr[id^=' + rowId + ']').remove();
            });
        }
        
        e.preventDefault();
    });
    $('.numeric').numeric().trigger('blur');
    // on submit, replace comma on numeric input field
    $('#StudentClass').submit(function() {
        $('.numeric').each(function() {
            this.value = this.value.replace(/,/gi, '');
        });
    });
});

/**
 * Function to format new tr after
 * #add_row is clicked
 * @param dom tr cloned row to be formatted before appended into tbody
 * @return dom tr after formatted
 */
function preformatNewRow(tr) {    
    // get row-xth
    // and increment it
    var oldId = tr.attr('id').substr(1)*1;
    var newId = oldId+1;
    $(tr).attr('id', 'r' + newId);
    
    // rel is
    $(tr).attr('rel', 'r' + newId);
    
    // change index of all input elements (list_Activities)
    $('.cb_payment:first', tr).attr('name', 'data[Student][' + 
        newId + '][id]');
    $('.name:first', tr).attr('name', 'data[Student][' + 
        newId + '][Student][name]');
    $('.npm:first', tr).attr('name', 'data[Student][' + 
        newId + '][Student][npm]');
    $('.gender:first', tr).attr('name', 'data[Student][' + 
        newId + '][Student][gender]');
    
    return tr;
}
