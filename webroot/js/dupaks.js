$(function() {
    // force numeric on input.numeric
    if ( onAdd ) {
        $('.numeric').each(function() {
            this.value = '0';
        });
    }
    $('.numeric').numeric();
    
    // check existences of dupak_year
    // only on add action
    if ( onAdd ) {
        checkExistences( $('#DupakDupakDateYear').val() );
        $('.dupak_date').change(function() {
            checkExistences( $('#DupakDupakDateYear').val() );
        });
    }
});

function checkExistences(dupak_year) {
    $.get(chkTransURL + '/' + dupak_year,
    function(existences) {
        if ( existences*1 && onAdd ) {
            $('#add').attr('disabled', true);
            $('#hd').html('<div class="flash_box flash_error">DUPAK untuk tahun ini sudah ada</div>');
            $.scrollTo('#hd', 'fast')
        } else {
            $('#add').attr('disabled', false);
            $('#hd').html('');
        }
    });
}
