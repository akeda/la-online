var trClone; // first loaded tr is cloned
$(function() {
    var tableScope = $('#students');
    var tableScopeBody = $('tbody:first', tableScope);
    
    // add row
    $('#add_row').live("click", function(e) {
        var lastRow = $('tr.row_student:last', tableScope);
        
        // make sure exists one row on tableScope
        if ( lastRow.length ) {
            // preformating the clone
            newRow = preformatNewRow(lastRow.clone());
            
            // cache for later use
            if ( typeof(trClone) == 'undefined' ) {
                trClone = newRow;
            }

            lastRow.after(newRow);
        }
    });
    
    // delete row
    $('#del_row').click(function(e) {
        var totalCb = $('.cb_student').length;
        var totalChecked = $('.cb_student:checked').length;
        
        if ( totalChecked == 0 ) {
            alert('Centang mahasiswa yang ingin Anda hapus pada kotak\n'+
                  'kecil sebelah kiri nama mahasiswa'
            );
            return false;
        }
        
        if ( totalChecked < totalCb ) {
            $('.cb_student:checked').each(function(idx) {
                var rowId = $(this).parent().parent().attr('id');
                $('tr[id^=' + rowId + ']').remove();
            });
        } else {
            alert('Anda tidak dapat mahasiswa ini,\n' +
                  'karena harus ada 1 mahasiswa yang tersisa, jika ada\n' +
                  'lebih dari 1 mahasiswa, maka mahasiswa ini dapat dihapus'
            );
            return false;
        }
        
        e.preventDefault();
    });
});

/**
 * Function to format new tr after
 * #add_row is clicked
 * @param dom tr cloned row to be formatted before appended into tbody
 * @return dom tr after formatted
 */
function preformatNewRow(tr) {    
    // get row-xth
    // and increment it
    var oldId = tr.attr('id').substr(1)*1;
    var newId = oldId+1;
    $(tr).attr('id', 'r' + newId);
    
    // rel is
    $(tr).attr('rel', 'r' + newId);
    
    // change index of all input elements (list_Activities)
    $('.cb_student:first', tr).attr('name', 'data[Student][' + 
        newId + '][id]').val('');
    $('.name:first', tr).attr('name', 'data[Student][' + 
        newId + '][Student][name]').val('');
    $('.npm:first', tr).attr('name', 'data[Student][' + 
        newId + '][Student][npm]').val('');
    $('.gender:first', tr).attr('name', 'data[Student][' + 
        newId + '][Student][gender]').val('');
    
    return tr;
}
